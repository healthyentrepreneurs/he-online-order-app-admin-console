# HE-ADMIN-Frontend

Health Enterpreneurs (https://headmin-staging.herokuapp.com).

## Available Scripts

In the project directory, you can run:

### `yarn run dev` or `npm run dev`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles the app in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

**Note: For security reasons, we dealt away with inline css and js by setting INLINE_RUNTIME_CHUNK=false**
