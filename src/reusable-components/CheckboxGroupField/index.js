import React from "react";
import { Form, Checkbox } from "antd";

const FormItem = Form.Item;

export const CheckboxGroupField = ({
  label,
  field,
  form: { touched, errors },
  ...props
}) => {
  const errorMsg = errors[field.name];
  return (
    <FormItem validateStatus={errorMsg ? "error" : ""} help={errorMsg}>
      <Checkbox.Group
        {...props}
        style={{
          display: "block",
          width: "150px",
          lineHeight: "30px",
        }}
        name={field.name}
        options={props.options}
        value={props.value}
        onChange={(checkedValues) => {
          props.setFieldValue(field.name, checkedValues);
        }}
      />
    </FormItem>
  );
};
