import React from 'react';
import { Button as AntButton } from 'antd';

import '../../styles/buttons.less';

const Button = props => {
    return (
        <AntButton
            className={props.type === "alt" ? "he-alt-button" : "he-button"}
            onClick={props.onClick}
            loading={props.loading}
            htmlType={props.htmlType && props.htmlType}
            {...props}
        >
            <span className="btn-text">{props.text}</span>
        </AntButton>
    )
}

export default Button;
