import React from 'react';
import { Form,  Input, } from 'antd';
import { TimePicker } from "@jbuschke/formik-antd";
import { Formik, Field } from 'formik';
import moment from 'moment';

const FormItem = Form.Item;

const format = 'HH:mm';

export const TimeField = ({ label, field, form: { touched, errors }, ...props }) => {
	const errorMsg =  errors[field.name];
	
	return (
	  <FormItem
	    label={label}
	    validateStatus={ errorMsg ? 'error': ''}
	    help={errorMsg}>
	    <TimePicker
	      name={field.name} 
	      format={format} 
	      size={props.size}
	      defaultValue={props.defaultValue}
	    />
	  </FormItem>
	);
}