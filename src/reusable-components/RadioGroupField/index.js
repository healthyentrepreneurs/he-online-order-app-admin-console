import React from 'react';
import { Form, Radio } from 'antd';
import { Formik, Field } from 'formik';

const FormItem = Form.Item;

export const RadioGroupField = ({ label, field, form: { touched, errors }, ...props }) => {
	const errorMsg =  errors[field.name];
	const radioStyle = {
    display: 'block',
    height: '30px',
    lineHeight: '30px',
  };
	return (
	  <FormItem
	  	label={label}
	    validateStatus={ errorMsg ? 'error': '' }
	    help={errorMsg}>
		  <Radio.Group
		  	name={field.name}
		  	value={props.value}
		  	onChange={e => {
          props.setFieldValue(field.name, e.target.value)
        }}>
        { props.options.map((option, index) => (
        		<Radio style={radioStyle} key={index} value={option.value}>{option.label}</Radio>
		  		))
		  	}
	    </Radio.Group>
	  </FormItem>
	);
}

