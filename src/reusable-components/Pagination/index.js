import React from "react";
import { Pagination as RNPagination } from "antd";

const Pagination = ({ page = 0, totalCount = 0, defaultPageSize=20, ...props }) => {
  console.log('pagination page', page, 'total count', totalCount)
  const onChange = (page) => {
    props.modifyPagination({ currentPage: page });
    props.handlePagination(page);
  };

  return (
    <>
      <RNPagination
        total={parseInt(totalCount) || 0}
        onChange={onChange}
        defaultCurrent={1}
        defaultPageSize={defaultPageSize}
        current={page}
        showSizeChanger={false}
      />
    </>
  );
};

export default Pagination;
