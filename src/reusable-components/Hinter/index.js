import React from 'react';
import { Card } from 'antd';

export const Hinter = ({...props}) => {
	return (
    <div className="hint-container">
      <Card 
        size="small" 
        title={props.title} 
        style={{ fontSize: 12 }}>
        <div style={{ fontSize: 10 }}>
        	{ props.messages && props.messages.map((message, index) => {
        			return (
        				<p key={index}>{message}</p>
        			)
          	})
          }
        </div>
      </Card>
    </div>
	)
}
