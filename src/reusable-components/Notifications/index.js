import { notification } from "antd";

export const OpenNotificationWithIcon = (type, header, description) => {
	notification[type]({
		message: header,
		description,
	});
};
