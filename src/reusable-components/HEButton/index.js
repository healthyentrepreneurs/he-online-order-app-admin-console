import { Button } from 'antd'
import React from 'react'
import '../../styles/he-button.less';


const HeButton = (props) => {
  const {
    bgColor
  } = props
  return (
    <Button
      className={props.type === "alt" ? "my-he-alt-button" : "my-he-button"}
      onClick={props.onClick}
      loading={props.loading}
      htmlType={props.htmlType && props.htmlType}
      style={{ backgroundColor: bgColor ? bgColor : '' }}
      {...props}
    >
      <span className="btn-text">{props.text}</span>
    </Button>
  )
}


export default HeButton