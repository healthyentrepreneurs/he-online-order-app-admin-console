import React from 'react';

export const StaticHinter = (props) => {
  return (
    <div className="static_hinter">
      <p>{props.text}</p>
    </div>
  )
}