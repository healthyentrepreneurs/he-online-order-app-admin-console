import React from 'react';
import { Field, getIn } from 'formik';


export const ErrorMessage = ({ name }) => (
  <Field
    name={name}
    render={({ form }) => {
      const error = getIn(form.errors, name);
      const touch = getIn(form.touched, name);
      return (
          //touch &&
          <span style={{color: "red"}}>{ error ? error : null}</span>
      )
    }}
  />
);

