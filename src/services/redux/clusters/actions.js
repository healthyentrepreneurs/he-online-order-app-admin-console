import { createActions } from "reduxsauce";

export const { Types, Creators } = createActions(
  {
    fetchClusters: ["query"],
    fetchClustersSuccess: ["responseData", "totalCount"],
    fetchClustersFailure: ["error"],
    resetClusters: null
  }
)