import { createActions } from "reduxsauce";

export const { Types, Creators } = createActions(
  {
    fetchDistricts: ["query"],
    fetchDistrictsSuccess: ["responseData", "totalCount"],
    fetchDistrictsFailure: ["error"],
    resetDistricts: null
  }
)