import { apiRequest } from "../apiRequest";

export const fetchPaymentTerms = (query) => {
  const queryStr = query ? `?${query}` : ""
	return apiRequest("GET", `/accounting/payment_terms${queryStr}`);
};