import { apiRequest } from "../apiRequest";
import { PAYMENT_METHODS_ENDPOINT } from "../urls";

export const fetchPaymentMethods = () => {
	return apiRequest("GET", PAYMENT_METHODS_ENDPOINT);
};
