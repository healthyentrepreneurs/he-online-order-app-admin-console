import React, { useEffect, useState } from 'react'
import Filters from "./filters";
import List from "./list";

import PropTypes from "prop-types";

const ListComponent = (props) => {
  const { fetchOrders, resetPagination } = props;
  const [state, setState] = useState({
    isVisible: false,
    userID: null,
    searchTerm: null,
    status: undefined,
    fromDate: undefined, 
    endDate: undefined,
  })

  useEffect(() => {
    resetPagination();
    fetchOrders();
    //eslint-disable-next-line
  }, [])

  return (
    <>
      <Filters
        handleChange={(fromDate, endDate, status, searchTerm) => {
          setState({ ...state, searchTerm, status, fromDate, endDate });
          resetPagination();
          fetchOrders({ 
            page: 1,
            search_term: searchTerm,
            from_date: fromDate,
            end_date: endDate,
            status,
          });
        }}
        {...props}
      />
      <div className="filters-list">
        <List
          searchTerm={state?.searchTerm}
          fromDate={state?.fromDate}
          endDate={state?.endDate}
          status={state?.status}
          {...props}
        />
      </div>
    </>
  )
}

ListComponent.propTypes = {
  isloading: PropTypes.bool,
};

export default ListComponent