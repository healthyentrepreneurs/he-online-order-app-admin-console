import React, { useEffect } from "react";
import { Row, Col, Card, Spin, Breadcrumb } from "antd";
import { connect } from "react-redux";
import moment from "moment";

import { Creators } from "../../../../services/redux/orders/actions";
import "../../../../styles/orders.less";
import { history } from "../../../../util/helpers/browserHistory";
import { Helmet } from "react-helmet";
import pageTitles from "../../../../util/page-titles";

const getSpanValue = (index, dataLength) => {
  if (dataLength === 1) {
    return 24;
  }

  if (dataLength === 2) {
    return 12;
  }

  if (dataLength > 2 && dataLength - index === 1) {
    return 12;
  }

  return 12;
};

const renderDetail = (data) => {
  if (!data) return "";
  return (
    <React.Fragment>
      <Card className="he-order-details">
        <Row justify="end" gutter={2}>
          <Col span={8}>
            <p className="detail-title">Order Name</p>
            <p className="detail-info">{data.name}</p>
          </Col>
          <Col span={8}>
            <p className="detail-title">Order Date</p>
            <p className="detail-info">
              {moment(data.created_at).format("Do MMMM, YYYY")}
            </p>
          </Col>
          <Col span={8} align="left">
            <p className="detail-title">Order Status</p>
            <p className="detail-info">{data.order_status}</p>
          </Col>
        </Row>
        <div className="delivery-wrapper" />
        <Row>
          <Col span={8} align="left">
            <p className="detail-title">Delivery Date</p>
            <p className="detail-info">
              {moment(data.delivery_date).isValid()
                ? moment(data.delivery_date).format("Do MMMM, YYYY")
                : "No delivery date"}
            </p>
          </Col>
        </Row>
        {data.payment_details ? (
          <React.Fragment>
            <div className="delivery-wrapper" />
            <p className="payment-details">Payment Details</p>
            <Row>
              <Col span={8} align="left">
                <p className="detail-title">Transaction Id</p>
                <p className="detail-info">{data.payment_details.transaction_id}</p>
              </Col>
              <Col span={8} align="left">
                <p className="detail-title">Payment Provider</p>
                <p className="detail-info">
                  {data.payment_details.payment_method?.carrier}
                </p>
              </Col>
              <Col span={8} align="left">
                <p className="detail-title">Phone Number</p>
                <p className="detail-info">{data.payment_details.phone_number}</p>
              </Col>
            </Row>
          </React.Fragment>
        ) : null}
      </Card>
      <div className="qty-divider" />
      <Row className="product-list-wrapper">
        {data.order_line_items &&
          data.order_line_items.length &&
          data.order_line_items.map((qty, index) => (
            <Col
              span={getSpanValue(index, data.order_line_items.length)}
              key={qty.id}
              className="product-list-col"
            >
              <p className="product-name">{qty.name}</p>
              <p className="product-price">
                {qty.currency_id[1]}{" "}
                {Number(qty.price_total.toFixed(1)).toLocaleString()}
              </p>
              <p className="product-name">Quantity: {qty.product_uom_qty}</p>
            </Col>
          ))}
      </Row>
      <Row className="product-list-wrapper">
        <Col span={12} className="product-list-col">
          <p>Total</p>
        </Col>
        <Col span={12} className="product-list-col">
          <p className="product-price" style={{ float: "right" }}>
            {data.currency}{" "}
            {Number(
              data.order_line_items
                .reduce((a, { price_total }) => a + price_total, 0)
                .toFixed(1)
            ).toLocaleString()}
          </p>
        </Col>
      </Row>
    </React.Fragment>
  );
};

const OrderDetails = (props) => {
  const { pageTitle } = props
  useEffect(() => {
    props.fetchDetail(props.location.pathname.split("/")[3]);
    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      <Helmet>
        <title>{pageTitle}</title>
      </Helmet>
      <Breadcrumb className="bread-crumb-container">
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>
          <a
            href="true"
            onClick={(e) => {
              e.preventDefault();
              history.push("/app/orders");
            }}
          >
            Orders List
          </a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Order Details</Breadcrumb.Item>
      </Breadcrumb>
      {props.loading ? <Spin /> : renderDetail(props.data)}
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.orders.fetchingDetail,
    data: state.orders.detail,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchDetail: (id) => {
      dispatch(Creators.fetchOrderDetail(id));
    },
  };
};

OrderDetails.defaultProps = {
  pageTitle: pageTitles.orderView
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetails);
