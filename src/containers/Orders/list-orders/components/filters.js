import React, { useState } from "react";
import { Input, Select, Row, Col, DatePicker } from "antd";
import moment from "moment";

const { Option } = Select;

const Filters = ({ isloading, handleChange, ...props }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [start_date, setStartDate] = useState("");
  const [end_date, setEndDate] = useState("");
  const [status, setStatus] = useState("");

  const onChange = (e) => {
    const val = e.target.value;
    setSearchTerm(val);
    handleChange(start_date, end_date, status, val);
  };

  return (
    <>
      <div>
        <Row gutter={32}>
          <Col sm={24} md={8}>
            <h1 className="header">Orders</h1>
          </Col>
          <Col offset={1} md={15} sm={12}>
            <div>
              <Row gutter={5}>
                <Col
                  md={2}
                  sm={12}
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <h4 style={{ padding: "0", margin: "0" }}>Filter</h4>
                </Col>
                <Col md={5}>
                  <Select
                    allowClear={true}
                    name="status"
                    size="large"
                    style={{ width: "100%" }}
                    loading={isloading}
                    showSearch
                    placeholder="status"
                    optionFilterProp="children"
                    onChange={(value) => {
                      setStatus(value);
                      handleChange(start_date, end_date, value, searchTerm);
                    }}
                    filterOption={(input, option) =>
                      option.props.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    <Option value="confirmed">Confirmed</Option>
                    <Option value="not confirmed">Not confirmed</Option>
                    <Option value="delivered">Delivered</Option>
                    <Option value="not delivered">Not delivered</Option>
                    <Option value="canceled">Canceled</Option>
                  </Select>
                </Col>
                <Col md={5}>
                  <DatePicker
                    name="from_date"
                    format="YYYY-MM-DD HH:mm:ss"
                    showTime={{ defaultValue: moment("00:00:00", "HH:mm:ss") }}
                    size="large"
                    className="filters-date-picker"
                    onChange={(date, dateString) => {
                      setStartDate(dateString);
                      handleChange(dateString, end_date, status, searchTerm);
                    }}
                    placeholder="Start date"
                    disabledDate={(current) => {
                      // disable future dates
                      const today = moment();
                      return current && current > moment(today, "YYYY-MM-DD");
                    }}
                  />
                </Col>
                <Col md={5}>
                  <DatePicker
                    name="from_date"
                    format="YYYY-MM-DD HH:mm:ss"
                    showTime={{ defaultValue: moment("00:00:00", "HH:mm:ss") }}
                    size="large"
                    className="filters-date-picker"
                    onChange={(date, dateString) => {
                      setEndDate(dateString);
                      handleChange(start_date, dateString, status, searchTerm);
                    }}
                    placeholder="End date"
                    disabledDate={(current) => {
                      // disable future dates
                      const today = moment();
                      return current && current > moment(today, "YYYY-MM-DD");
                    }}
                  />
                </Col>
                <Col md={7} sm={7}>
                  <Input
                    name="search_term"
                    size="large"
                    className="search-input"
                    style={{ width: "100%" }}
                    placeholder="Search..."
                    onChange={onChange}
                    value={searchTerm}
                    autoComplete="off"
                    allowClear={true}
                  />
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Filters;
