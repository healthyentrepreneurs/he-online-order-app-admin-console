import React, { Component } from "react";
import Filters from "./filters";
import List from "./list";

import PropTypes from "prop-types";

export default class ListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      userID: null,
      searchTerm: null,
      status: undefined,
      fromDate: undefined,
      endDate: undefined,
    };
  }

  async componentDidMount() {
    const { fetchOrders, resetPagination } = this.props;
    resetPagination();
    fetchOrders();
  }

  render() {
    const { fetchOrders, resetPagination } = this.props;
    const { searchTerm, status, fromDate, endDate } = this.state;

    return (
      <>
        <Filters
          handleChange={(fromDate, endDate, status, searchTerm) => {
            this.setState({ searchTerm, status, fromDate, endDate });
            resetPagination();
            fetchOrders({
              page: 1,
              search_term: searchTerm,
              from_date: fromDate,
              end_date: endDate,
              status,
            });
          }}
          {...this.props}
        />
        <div className="filters-list">
          <List
            searchTerm={searchTerm}
            fromDate={fromDate}
            endDate={endDate}
            status={status}
            {...this.props}
          />
        </div>
      </>
    );
  }
}

ListComponent.propTypes = {
  isloading: PropTypes.bool,
};
