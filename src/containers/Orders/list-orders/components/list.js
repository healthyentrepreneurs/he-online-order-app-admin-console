import React from "react";
import { Row, Col, Table } from "antd";
import { EyeOutlined } from "@ant-design/icons";
import Spinner from "../../../../reusable-components/Spinner";
import Pagination from "../../../../reusable-components/Pagination";
import { history } from "../../../../util/helpers/browserHistory";
import get from "lodash/get";
import moment from "moment";
import { Helmet } from "react-helmet";
import pageTitles from "../../../../util/page-titles";

const columns = (props) => [
  {
    title: "Date Created",
    dataIndex: "created_at",
    key: "created_at",
    render: (text, row) => {
      return moment(row.created_at).format("YYYY-MM-DD");
    },
  },
  {
    title: "Order Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Items",
    dataIndex: "order_line_items",
    key: "order_line_items",
    width: "35%",
    render: (text, row) => {
      return row.order_line_items.length > 0 ? (
        <span className="item-table-details">
          {row.order_line_items.reduce((acc, cur) => {
            return (acc = acc ? acc + "," + cur.name : cur.name);
          }, "")}
        </span>
      ) : (
        "N/A"
      );
    },
  },
  {
    title: "Customer",
    dataIndex: "customer.username",
    key: "customer.username",
    render: (text, row) => {
      return get(row.customer, "username");
    },
  },
  {
    title: "Status",
    dataIndex: "order_status",
    key: "order_status",
    render: (text, row) => {
      if (row.order_status === "confirmed" || row.order_status === "delivered")
        return <span style={{ color: "green" }}>{row.order_status}</span>;
      else return <span style={{ color: "red" }}>{row.order_status}</span>;
    },
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
    align: "center",
    render: (text, row) => {
      return (
        <>
          <EyeOutlined
            onClick={() => history.push(`/app/order/${row.order_id}`)}
            style={{ color: "#A9B4C1" }}
            className="edit-icon"
          />
        </>
      );
    },
  },
];

const List = ({
  searchTerm,
  fromDate,
  endDate,
  status,
  orders,
  page,
  totalCount,
  isloading,
  ...props
}) => {
  const { pageTitle } = props
  return (
    <>
      <Helmet>
        <title>{pageTitle}</title>
      </Helmet>
      {isloading ? (
        <Spinner />
      ) : (
        <>
          <Row>
            <Col span={24}>
              {orders && (
                <Table
                  className="table he-table"
                  dataSource={orders}
                  pagination={false}
                  columns={columns(props)}
                  rowKey="name"
                />
              )}
            </Col>
          </Row>
          <Row>
            <Col offset={16} md={8}>
              <div className="filters-pagination">
                <Pagination
                  handlePagination={(page) => {
                    props.fetchOrders({
                      page,
                      search_term: searchTerm,
                      status,
                      from_date: fromDate,
                      end_date: endDate,
                    });
                  }}
                  page={page}
                  totalCount={totalCount}
                  modifyPagination={props.modifyPagination}
                />
              </div>
            </Col>
          </Row>
        </>
      )}
    </>
  );
};

List.defaultProps = {
  pageTitle: pageTitles.orderList
}

export default List;
