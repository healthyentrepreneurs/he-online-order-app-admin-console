import { connect } from "react-redux";
import { Creators } from "../../../services/redux/orders/actions";
import { Creators as PaginationCreators } from "../../../services/redux/pagination/actions";
import { Creators as AuthCreators } from "../../../services/redux/auth/actions";
import ListComponent from "./components";

const mapStateToProps = (state) => {
  const { orders, isloading, totalCount } = state.orders;
  return {
    orders,
    isloading,
    pagination: state.pagination,
    page: state.pagination.currentPage,
    totalCount,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchOrders: (query = { page: 1 }) => {
      dispatch(Creators.fetchOrders(query));
    },
    modifyPagination: (obj) => {
      dispatch(PaginationCreators.modifyPagination(obj));
    },
    resetPagination: () => {
      dispatch(PaginationCreators.reset());
    },
    saveDeviceToken: registration_id => {
      dispatch(AuthCreators.saveDeviceToken(registration_id))
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListComponent);
