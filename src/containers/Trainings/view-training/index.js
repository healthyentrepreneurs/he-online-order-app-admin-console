import { connect } from "react-redux";
import { Creators } from "../../../services/redux/trainings/actions";
import ViewTraining from "./components";

const mapStateToProps = (state) => {
  const {
    training,
    is_loading,
    errors,
    updating_training,
    update_training_success,
    training_products,
    free_products
  } = state.trainings;
  return {
    training,
    is_loading,
    errors,
    updating_training,
    update_training_success,
    training_products,
    free_products
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchTrainingById: (id) => {
      dispatch(Creators.fetchTrainingById(id));
    },
    updateTraining: (id, data) => {
      
      dispatch(Creators.updateTraining(id, data));
    },
    resetTraining: () => {
      dispatch(Creators.resetTraining());
    },
    addFreeProduct: (item) => {
      dispatch(Creators.addFreeProduct(item))
    },
    removeFreeProduct: (item) => {
      dispatch(Creators.removeFreeProduct(item))
    },
    editFreeProduct: (item) => {
      dispatch(Creators.editFreeProduct(item))
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewTraining);
