import React from 'react'
import { Row, Col, Table, Button } from "antd"
import { MinusOutlined } from '@ant-design/icons'
import moment from 'moment'

const columns = (props) => [
  {
    title: "Item Name",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Till when",
    dataIndex: "expiry_date",
    key: "expiry_date",
    render: (text) => moment(text).format("YYYY-MM-DD HH:mm:ss")
  },
  {
    title: "Actions",
    dataIndex: "operations",
    key: "operations",
    render: (text, row) => (
      <Row>
        <Col span={24}>
          <Button
            type="primary"
            onClick={event => {
              props.handleRemoveFreeProduct(row)
            }}
          >
            <MinusOutlined />
          </Button>
        </Col>
      </Row>
    )
  }
]

const List = (props) => {
  const { freeProducts, handleRemoveFreeProduct } = props
  return (
    <>
      <Table 
        columns={columns({ handleRemoveFreeProduct })}
        dataSource={freeProducts}
        rowKey="id"
        pagination={false}
      />
    </>
  )
}

export default List;