import React, { useState } from "react";
import { Input, Row, Col } from "antd";

const Filters = ({ isloading, handleChange, ...props }) => {
  const [searchTerm, setSearchTerm] = useState("");

  const onChange = (e) => {
    const val = e.target.value;
    setSearchTerm(val);
    handleChange(val);
  };

  return (
    <>
      <div>
        <Row>
          <Col
            md={8}
            style={{
              display: "flex",
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <b style={{ fontSize: "20px" }}>Products Visible</b>
          </Col>
          <Col md={16}>
            <Row>
              <Col offset={10} md={14} sm={10}>
                <Input
                  name="search_term"
                  size="large"
                  className="search-input"
                  style={{ width: "100%", float: "right" }}
                  placeholder="Search..."
                  onChange={onChange}
                  value={searchTerm}
                  autoComplete="off"
                  allowClear={true}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Filters;
