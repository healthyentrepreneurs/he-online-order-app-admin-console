import React, { useEffect, useState } from 'react'

import PropTypes from "prop-types"
import pageTitles from "../../../../util/page-titles";
import Spinner from "../../../../reusable-components/Spinner";
import { history } from "../../../../util/helpers/browserHistory";
import { Helmet } from "react-helmet";
import FreeProductList from "./FreeProductList";
import MyButton from "../../../../reusable-components/Button"
import { Row, Col, Breadcrumb, message } from 'antd';
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import AddFreeProductModal from "./AddFreeProductModal";
import EditFreeProductModal from "./EditFreeProductModal";
import moment from "moment"


const ViewTraining = (props) => {
  const {
    training,
    is_loading,
    fetchTrainingById,
    updating_training,
    pageTitle,
    free_products,
    training_products,
    addFreeProduct,
    removeFreeProduct,
    editFreeProduct,
    updateTraining
  } = props

  // const [addProductModalVisible, setAddProductModalVisible] = useState(false)
  const [addFreeProductModalVisible, setAddFreeProductModalVisible] = useState(false)
  const [editFreeProductModalVisible, setEditFreeProductModalVisible] = useState(false)
  const [selectedEditFreeProduct, setSelectedEditFreeProduct] = useState(null)
  // const [searchTerm, setSearchTerm] = useState(null)

  const params = useParams()

  useEffect(() => {
    if (Number(params?.id)) {
      fetchTrainingById(params.id)
    } else {
      message.error('Invalid training id!')
    }
    // eslint-disable-next-line 
  }, [params?.id])

  return (
    <>
      <Helmet>
        <title>{pageTitle}</title>
      </Helmet>
      {is_loading ? (
        <Spinner />
      ) : (
        <>
          <Row>
            <Col span={24} className="bread-crumb-container">
              {" "}
              <Breadcrumb>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>
                  <a
                    href="true"
                    onClick={(e) => {
                      e.preventDefault();
                      history.push("/app/trainings");
                    }}
                  >
                    Trainings
                  </a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Training Product Details</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <h1
                style={{
                  fontSize: "36px",
                  fontWeight: "bold",
                }}
              >
                {training?.display_name || "N/A"}(
                {training?.company?.name || "No company attached"})

              </h1>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <span style={{ fontWeight: "bold" }}>Promotions with Free Products</span>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col md={24} sm={24} xs={24}>
              <Row>
                <Col span={24}>
                  <FreeProductList
                    freeProducts={free_products}
                    onRemoveFreeProduct={(freeProduct) => {
                      removeFreeProduct(freeProduct)
                    }}
                    onEditFreeProduct={(freeProduct) => {
                      setEditFreeProductModalVisible(true)
                      setSelectedEditFreeProduct(freeProduct)
                    }}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
          <Row style={{ marginTop: 5 }}>
            <Col span={12} style={{ textAlign: "right" }}>
              <MyButton
                text="Add new"
                type="alt"
                onClick={() => {
                  setAddFreeProductModalVisible(true)
                }}
              />
            </Col>
            <Col span={12}>
              <MyButton
                loading={updating_training}
                text="SAVE"
                onClick={() => {
                  updateTraining(params.id, {
                    free_product_ids: free_products,
                  })
                }}
              />
            </Col>
          </Row>
          <AddFreeProductModal
              freeProducts={free_products}
              training_id={params.id}
              visible={addFreeProductModalVisible}
              onCancel={()=> {
                setAddFreeProductModalVisible(false)
              }}
              products={training_products}
              onOk={(freeProduct) => {
                addFreeProduct(freeProduct)
                setAddFreeProductModalVisible(false)
              }}
            />
            <EditFreeProductModal
              initialValues={{
                product_id: selectedEditFreeProduct?.odoo_id,
                expiry_date: moment(selectedEditFreeProduct?.expiry_date),
                min_order_value: selectedEditFreeProduct?.min_order_value || 0
              }}
              freeProducts={free_products}
              training_id={params.id}
              visible={editFreeProductModalVisible}
              selectedEditFreeProduct={selectedEditFreeProduct}
              onCancel={() => {
                setEditFreeProductModalVisible(false)
              }}
              products={training_products}
              onOk={(freeProduct) => {
                editFreeProduct(freeProduct)
                setEditFreeProductModalVisible(false)
                setSelectedEditFreeProduct(null)
              }}
            />
        </>
      )}
    </>
  )
}

ViewTraining.defaultProps = {
  pageTitle: pageTitles.userGroupView
}

ViewTraining.propTypes = {
  fetchUserGroupProducts: PropTypes.func,
};

export default ViewTraining