import React from "react";
import Spinner from "../../../../reusable-components/Spinner";

const Submit = ({ isloading, handleSubmit }) => {
  let buttonState = isloading ? <Spinner color="#ffffff" /> : "Submit";
  return (
    <>
      <button
        disabled={isloading}
        className="header-btn"
        style={{ float: "right", marginTop: "30px" }}
        onClick={() => handleSubmit()}
      >
        {buttonState}
      </button>
    </>
  );
};

export default Submit;
