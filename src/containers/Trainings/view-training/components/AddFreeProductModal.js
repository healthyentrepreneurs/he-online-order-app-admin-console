import { Modal, Row, Col, Form, Input, DatePicker, Select } from 'antd'
import React from 'react'
import moment from "moment"
import MyButton from "../../../../reusable-components/Button"

const formItemLayout = {
  layout: "vertical",
};

const extractIdsFromFreeProducts = (products) => {
  return products?.map(product => product.odoo_id) || []
}


const AddFreeProductModal = (props) => {
  const { 
    visible,
    onCancel,
    products,
    fetchingUserGroupProducts,
    training_id,
    freeProducts,
  } = props

  const [form] = Form.useForm()
  const initialValues = {
    product_id: "",
    expiry_date: "",
    min_order_value: 0
  }

  return (
    <>
      <Modal
        title="Add new free product"
        visible={visible}
        onCancel={()=> onCancel()}
        centered={true}
        footer={null}
        bodyStyle={{ padding: "20px 100px 70px 100px" }}
        width={630}
      >
        <Row>
          <Col md={24}>
            <Form
              form={form}
              initialValues={initialValues}
              {...formItemLayout}
              onFinish={(values) => {
                const item = {
                  id: values.product_id,
                  expiry_date: moment(values.expiry_date).format("YYYY-MM-DD HH:mm:ss"),
                  min_order_value: values.min_order_value || 0,
                  training: training_id,
                }
                props.onOk(item)
                form.resetFields()
              }}
            >
              <Form.Item
                label="Product name"
                name="product_id"
                rules={[
                  {
                    required: true,
                    message: "Please select a product"
                  }
                ]}
              >
                <Select
                  loading={fetchingUserGroupProducts}
                  showSearch
                  optionFilterProp="children"
                  filterOption={(input, option) => {
                    return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }}
                >
                  <Select.Option value="">Please select a product</Select.Option>
                  {
                    (products || [])
                      .filter(item=> !extractIdsFromFreeProducts(freeProducts || []).includes(item.id))
                      .map(product => (
                        <Select.Option key={product.id} value={product.id}>
                          {product.name}
                        </Select.Option>
                    ))
                  }
                </Select>
              </Form.Item>
              <Form.Item
                label="Till when"
                name="expiry_date"
                rules={[
                  {
                    required: true,
                    message: "Please select an expiry date"
                  }
                ]}
              > 
                <DatePicker 
                  format="YYYY-MM-DD HH:mm:ss"
                  showTime
                />
              </Form.Item>
              <Row>
                <Col span={24} style={{textAlign: "center"}}>
                  <span style={{fontWeight: "bold"}}>Condition</span>
                </Col>
              </Row>
              <Form.Item
                label="Min. Order Value"
                name="min_order_value"
              >
                <Input type="number" step="1" />
              </Form.Item>
              <MyButton 
                text="SAVE"
                htmlType="submit"
              />
            </Form>
          </Col>
        </Row>
      </Modal>
    </>
  )
}

export default AddFreeProductModal