import React, { Component } from "react";
import { Row, Col, Breadcrumb } from "antd";
import PropTypes from "prop-types";
import get from "lodash/get";
import Spinner from "../../../../reusable-components/Spinner";
import { history } from "../../../../util/helpers/browserHistory";
import ProductsList from "./products-list";
import Filters from "./filters";
import Submit from "./submit";
import { getUserDetails } from "../../../../services/api/axiosDefaults";
import pageTitles from "../../../../util/page-titles";
import { Helmet } from "react-helmet";
import PromotionMessageForm from "./PromotionMessageForm";
import FreeProductList from "./FreeProductList";
import MyButton from "../../../../reusable-components/Button"
import AddFreeProductModal from "./AddFreeProductModal";
import EditFreeProductModal from "./EditFreeProductModal";
import moment from "moment"

export default class ViewTraining extends Component {
  state = {
    searchTerm: null,
    addFreeProductModalVisible: false,
    editFreeProductModalVisible: false,
    selectedEditFreeProduct: null
  };
  componentDidMount() {
    const { fetchTrainingById, match, resetTraining, training } = this.props;
    resetTraining();
    fetchTrainingById(get(match, "params.id"));
  }

  extractIds = (products) => {
    let ids = [];
    ids = products.reduce((acc, cur) => {
      return (acc = [...acc, cur.id]);
    }, []);
    return ids;
  };
  

  render() {
    const {
      training,
      is_loading,
      errors,
      match,
      removeFreeProduct,
      pageTitle,
    } = this.props;

    return (
      <>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        {is_loading ? (
          <Spinner />
        ) : (
          <>
            <Row>
              <Col span={24} className="bread-crumb-container">
                {" "}
                <Breadcrumb>
                  <Breadcrumb.Item>Home</Breadcrumb.Item>
                  <Breadcrumb.Item>
                    <a
                      href="true"
                      onClick={(e) => {
                        e.preventDefault();
                        history.push("/app/trainings");
                      }}
                    >
                      Trainings
                    </a>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>Training Product Details</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <h1
                  style={{
                    fontSize: "36px",
                    fontWeight: "bold",
                  }}
                >
                  {training?.display_name || "N/A"} (
                  {training?.company || "No company attached"})
                </h1>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col md={24} sm={24} xs={24}>
                <Row>
                  <Col span={24}>
                    <FreeProductList
                      freeProducts={training?.freeProducts}
                      onRemoveFreeProduct={(freeProduct) => {
                        removeFreeProduct(freeProduct)
                      }}
                      onEditFreeProduct={(freeProduct) => {
                        this.setState({...this.state, editFreeProductModalVisible: true, selectedEditFreeProduct: freeProduct})
                      }}
                    />
                  </Col>
                </Row>
                <Row style={{marginTop: 5}}>
                  <Col span={12} style={{textAlign: "right"}}>
                    <MyButton
                      text="Add new"
                      type="alt"
                      onClick={() => {
                        this.setState({...this.state, addFreeProductModalVisible: true})
                      }}
                    />
                  </Col>
                  <Col span={12}>
                    <MyButton 
                      loading={updatingUserGroup}
                      text="SAVE"
                      onClick={()=> {
                        //const blackListedIds = this.extractIds(blackListedProducts);
                        this.props.updateUserGroup(get(match, "params.id"), 
                        {
                          free_product_ids: freeProducts,
                          // black_list_ids: blackListedIds
                        })
                      }}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
            {/* <AddFreeProductModal
              freeProducts={freeProducts}
              user_group_id={get(match, "params.id")}
              visible={this.state.addFreeProductModalVisible}
              onCancel={()=> {
                this.setState({...this.state, addFreeProductModalVisible: false})
              }}
              products={userGroupProducts}
              onOk={(freeProduct) => {
                addFreeProduct(freeProduct)
                this.setState({...this.state, addFreeProductModalVisible: false})
              }}
            />
            <EditFreeProductModal
              initialValues={{
                product_id: this.state.selectedEditFreeProduct?.odoo_id,
                expiry_date: moment(this.state.selectedEditFreeProduct?.expiry_date),
                min_order_value: this.state.selectedEditFreeProduct?.min_order_value || 0
              }}
              freeProducts={freeProducts}
              user_group_id={get(match, "params.id")}
              visible={this.state.editFreeProductModalVisible}
              selectedEditFreeProduct={this.state.selectedEditFreeProduct}
              onCancel={() => {
                this.setState({...this.state, editFreeProductModalVisible: false})
              }}
              products={userGroupProducts}
              onOk={(freeProduct) => {
                editFreeProduct(freeProduct)
                this.setState({...this.state, editFreeProductModalVisible: false, selectedEditFreeProduct: null})
              }}
            /> */}
          </>
        )}
      </>
    );
  }
}

ViewTraining.defaultProps = {
  pageTitle: pageTitles.userGroupView
}

ViewTraining.propTypes = {
  fetchUserGroupProducts: PropTypes.func,
};
