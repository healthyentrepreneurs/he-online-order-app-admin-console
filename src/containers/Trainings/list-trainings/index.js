import { connect } from 'react-redux';

import { Creators } from "../../../services/redux/trainings/actions";
import { Creators as PaginationCreators } from "../../../services/redux/pagination/actions";
import ListTrainings from './component';

const mapStateToProps = state => {
  const {
    is_loading,
    trainings,
    errors,
    sync_percent,
    tracking_trainings_sync,
    syncing,
    sync_success,
    sync_message,
    sync_task_id,
    last_sync_data,
    last_sync_time,
    last_sync_status,
    loading_last_sync,
    posting_sync,
    sync_data,
    total_count,
  } = state.trainings
  return {
    isFetchingTrainings: is_loading,
    data: trainings,
    error: errors,
    total_count,

    sync_data,
    posting_sync,
    last_sync_data,
    last_sync_time,
    last_sync_status,
    loading_last_sync,

    tracking_trainings_sync,
    syncing,
    sync_success,
    sync_message,
    sync_task_id,
    sync_percent,

    pagination: state.pagination,
    page: state.pagination.currentPage,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    resetPagination: () => {
      dispatch(PaginationCreators.reset());
    },
    modifyPagination: (obj) => {
      dispatch(PaginationCreators.modifyPagination(obj));
    },
    getTrainings: (query) => {
      dispatch(Creators.fetchTrainings(query))
    },
    syncTrainings: query => dispatch(Creators.syncTrainings(query)),
    getLastTrainingsSync: () => dispatch(Creators.getLastTrainingsSync()),
    trackTrainingsSync: (taskId, query) => dispatch(Creators.trackTrainingsSync(taskId, query))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListTrainings)
