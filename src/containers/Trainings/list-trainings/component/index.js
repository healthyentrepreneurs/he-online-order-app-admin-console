import React, { useEffect, useState } from 'react';
import { Table, Button as AntButton, Spin, Col, Row, message, Modal } from 'antd';
import { EditOutlined, SyncOutlined, EyeOutlined } from '@ant-design/icons';
import moment from 'moment';
import '../../../../styles/staff.less'
import { history } from '../../../../util/helpers/browserHistory';
import { Helmet } from 'react-helmet';
import pageTitles from '../../../../util/page-titles';
import HeButton from '../../../../reusable-components/HEButton';
import SyncTrainingProgressModal from './sync-training-progress-modal';
import SyncTrainingError from './sync-training-error';
import LastTrainingSyncStatus from './last-training-sync-status';
import Pagination from '../../../../reusable-components/Pagination';

const columns = [
  {
    title: 'Date Creatd',
    dataIndex: 'date_created',
    align: 'left',
    width: '10%'
  },
  {
    title: 'Name',
    dataIndex: 'name',
    align: 'left',
    width: '15%'
  },
  {
    title: 'Company',
    dataIndex: 'company',
    align: 'left',
    width: '25%'
  },
  {
    title: 'Action',
    dataIndex: 'action',
    align: 'center',
    width: '10%'
  }
];

const TrainingList = props => {
  const {
    pageTitle,
    data,
    isFetchingTrainings,
    getTrainings,
    total_count,
    page,

    syncTrainings,
    sync_status,
    trackTrainingsSync,
    getLastTrainingsSync,
    sync_percent,

    posting_sync,
    last_sync_data,
    last_sync_time,
    last_sync_status,

    tracking_trainings_sync,
    loading_last_sync,
    syncing,
    sync_message,
    sync_task_id,
    resetPagination

  } = props

  const [syncStatusVisible, setSyncStatusVisible] = useState(false)
  const [syncErrorVisible, setSyncErrorVisible] = useState(false)

  const onDisplaySyncError = () => {
    setSyncErrorVisible(true)
    //getLastTrainingsSync()
  }

  useEffect(() => {
    resetPagination();
    getLastTrainingsSync()
    getTrainings({page: 1});
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    let myInterval = setInterval(() => {
      if (syncing && sync_task_id && !tracking_trainings_sync) {
        trackTrainingsSync(sync_task_id, {})
      }
    }, 20000)
    return () => {
      clearInterval(myInterval)
    }
    //eslint-disable-next-line
  }, [syncing, sync_task_id, sync_status, tracking_trainings_sync])

  useEffect(() => {
    let lastSyncInterval = setInterval(() => {
      if(!syncing) {
        getLastTrainingsSync()
      }
    }, 20000)
    return () => {
      clearInterval(lastSyncInterval)
    }
    //eslint-disable-next-line
  }, [loading_last_sync, last_sync_status, last_sync_time, syncing])


  const actions = (id, dd) => {
    return (
      <div className="action-icon">
        <AntButton type="text" onClick={() => history.push(`/app/trainings/${id}`, { dd })}>
          <EditOutlined style={{ color: '#A9B4C1' }} />
        </AntButton>
      </div>
    )
  }

  const formattedTrainings = () => {
    let trainingsData = []
    if (data) {
      // eslint-disable-next-line
      data?.map((training, index) => {
        trainingsData.push({
          key: index,
          date_created: moment(training.created_at).format("YYYY-MM-DD"),
          name: training.name,
          company: training.company?.name,
          action: actions(training.training_id, training)
        })
      })
    }
    return trainingsData
  }
  return (
    <>
      <Helmet>
        <title>{pageTitle}</title>
      </Helmet>
      <div className="staff-list">
        <Row className="header">
          <Col span={24} style={{ display: "flex", flexDirection: "row", justifyContent: "flex-start", alignItems: "center" }}>
            <div><h4 className="title">Trainings</h4></div>
            <div style={{ marginLeft: 10 }}>
              <HeButton
                style={{ width: "50px" }}
                text={<SyncOutlined spin={posting_sync || syncing} />}
                onClick={() => {
                  setSyncStatusVisible(true)
                  if (!syncing) {
                    syncTrainings({})
                  } else {
                    message.info("Training sync in progress")
                  }
                }}
              />
            </div>
            <div>
              <LastTrainingSyncStatus
                lastSyncTime={last_sync_time}
                lastSyncData={last_sync_data}
                lastSyncStatus={last_sync_status}
                onDisplayError={onDisplaySyncError}
              />
            </div>
            <div>
              <HeButton
                style={{ width: "50px" }}
                onClick={() => onDisplaySyncError()}
                text={<EyeOutlined />}
                type="alt"
              //disabled={!lastSyncData}
              />
            </div>
          </Col>
        </Row>
        {isFetchingTrainings ? <Spin /> :
          <>
            <Row>
              <Col span={24}>
                <Table
                  className="he-table"
                  columns={columns}
                  dataSource={formattedTrainings()}
                  pagination={false}
                  align="center"
                />
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <div className="filters-pagination">
                  <Pagination 
                    handlePagination={(page) => {
                      getTrainings({
                        page,
                      });
                    }}
                    page={page}
                    totalCount={total_count}
                    defaultPageSize={20}
                    modifyPagination={props.modifyPagination}
                  />
                </div>
              </Col>
            </Row>
          </>
        }
      </div>

      <SyncTrainingProgressModal
        visible={syncStatusVisible}
        onCancel={() => {
          setSyncStatusVisible(false)
          window.location.reload()
        }}
        loading={syncing}

        percent={sync_percent}
        status='normal'
        message={sync_message}
      />

      <Modal
        visible={syncErrorVisible}
        title="Sync Error Details"
        onCancel={() => setSyncErrorVisible(false)}
        footer={null}
        style={{ minWidth: '800px', resize: 'none' }}
      >
        <SyncTrainingError
          data={last_sync_data}
        />
        <Row>
          <Col span={24} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <span>Timestamp: {moment(last_sync_time).format('DD-MM-YYYY hh:mm A')}</span>
          </Col>
        </Row>
      </Modal>
    </>
  )
}

TrainingList.defaultProps = {
  pageTitle: pageTitles.trainingsList
}

export default TrainingList;
