import React from 'react'
import PropTypes from "prop-types"
import { Col, Row } from 'antd'

const getErrorsList = (data) => {
  return data?.failed || []
}

const SyncTrainingError = ({
  data
}) => {
  
  return (
    <Row>
      <Col span={24}>
        <ul>
          {getErrorsList(data).map(err => (
            <li>{err}</li>
          ))}
        </ul>
      </Col>
    </Row>
  )
}

SyncTrainingError.propTypes = {
  country: PropTypes.string,
  errors: PropTypes.array
}
export default SyncTrainingError