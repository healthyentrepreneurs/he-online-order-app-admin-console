import { connect } from 'react-redux';

import { Creators } from "../../../services/redux/user_groups/actions";
import ListUserGroups from './component';

const mapStateToProps = state => {
    return {
        isFetchingUserGroups: state.user_groups.isFetchingUserGroups,
        data: state.user_groups.userGroups,
        error: state.user_groups.userGroupsFetchError
    }   
}

const mapDispatchToProps = dispatch => {
    return {
        getUserGroups: () => {
            dispatch(Creators.fetchUserGroups())
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListUserGroups)
