import React, { useEffect } from 'react';
import { Table, Button as AntButton, Spin } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import moment from 'moment';

import '../../../../styles/staff.less'
import { history } from '../../../../util/helpers/browserHistory';
import { Helmet } from 'react-helmet';
import pageTitles from '../../../../util/page-titles';

const columns = [
    {
        title: 'Date Creatd',
        dataIndex: 'date_created',
        align: 'left',
        width: '10%'
    },
    {
      title: 'Name',
      dataIndex: 'name',
      align: 'left',
      width: '15%'
    },
    {
      title: 'Company',
      dataIndex: 'company',
      align: 'left',
      width: '25%'
    },
    {
        title: 'Action',
        dataIndex: 'action',
        align: 'center',
        width: '10%'
    }
];

const UserGroupList = props => {
    const { pageTitle } = props
    useEffect(() => {
        props.getUserGroups();
        // eslint-disable-next-line
    }, []);

    const actions = (id, data) => {
        return (
            <div className="action-icon">
                <AntButton type="text" onClick={() => history.push(`/app/user-group/${id}`, { data })}>
                <EditOutlined style={{ color: '#A9B4C1' }} />
                </AntButton>
            </div>
        )
    }

    const formattedUserGroups = () => {
        const { data } = props;
        let userGroupsData = []
        if (data && data.count) {
            // eslint-disable-next-line
            data.results.map((user_group, index) => {
                userGroupsData.push({
                    key: index,
                    date_created: moment(user_group.date_created).format("YYYY-MM-DD"),
                    name: user_group.name,
                    company: user_group.company?.name,
                    action: actions(user_group.usergroup_id, user_group)
                })
            })
        }
        return userGroupsData
    }
    return (
        <>
            <Helmet>
                <title>{pageTitle}</title>
            </Helmet>
            <div className="staff-list">
                <div className="header">
                    <h4 className="title">User Groups</h4>
                </div>
                {props.isFetchingUserGroups ? <Spin /> : 
                    <Table
                        className="he-table"
                        columns={columns}
                        dataSource={formattedUserGroups()}
                        pagination={false}
                        align="center"
                    />
                }
            </div>
        </>
    )
}

UserGroupList.defaultProps = {
    pageTitle: pageTitles.userGroupList
}

export default UserGroupList;
