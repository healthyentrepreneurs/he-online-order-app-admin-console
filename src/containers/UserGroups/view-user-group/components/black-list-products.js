import React from "react";
import { Row, Col, Table, Button } from "antd";
import { MinusOutlined } from "@ant-design/icons";
import Spinner from "../../../../reusable-components/Spinner";
import get from "lodash/get";
import { getUserDetails } from "../../../../services/api/axiosDefaults";

const columns = (props) => [
  {
    title: "Item Name",
    dataIndex: "name",
    key: "name",
    width: "60%",
  },
  {
    title: "Item Code",
    dataIndex: "code",
    key: "code",
    width: "30%",
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
    align: "center",
    width: "10%",
    render: (text, row) => {
      return (
        <>
          <Button
            disabled={
              parseInt(get(props.company, "id")) ===
              parseInt(get(getUserDetails(), "country.id"))
                ? false
                : true
            }
            type="primary"
            onClick={() => props.removeFromBlackList(row)}
          >
            <MinusOutlined style={{ color: "white" }} />
          </Button>
        </>
      );
    },
  },
];

const List = ({
  searchTerm,
  isFetchingUserGroupProducts,
  blackListedProducts = [],
  ...props
}) => {
  return (
    <>
      {isFetchingUserGroupProducts ? (
        <Spinner />
      ) : (
        <>
          <Row>
            <Col span={24}>
              {blackListedProducts && (
                <Table
                  className="table he-table"
                  dataSource={blackListedProducts}
                  pagination={false}
                  columns={columns(props)}
                  size="small"
                  rowKey="id"
                />
              )}
            </Col>
          </Row>
        </>
      )}
    </>
  );
};

export default List;
