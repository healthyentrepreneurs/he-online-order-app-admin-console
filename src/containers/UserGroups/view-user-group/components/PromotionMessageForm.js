import React from 'react'
import { Row,Col, Input, Checkbox, DatePicker } from "antd"
import { Form } from 'antd'
import MyButton from "../../../../reusable-components/Button"
import moment from "moment"
import { useForm } from 'antd/lib/form/Form'

const formItemLayout = {
  labelCol: {
    span: 24,
  },
  wrapperCol: {
    span: 24,
  },
  layout: "vertical",
};

const PromotionMessageForm = (props) => {
  const { title, savePromotion, promotionMessage, submitting } = props
  const [ form ] = useForm()

  const initialValues = {
    message: promotionMessage?.message,
    expiry_date: moment(promotionMessage?.expiry_date),
    has_expiry: promotionMessage?.has_expiry
  }

  return (
    <>
      <Row>
        <Col span={24}>
          <span style={{ fontWeight: "bold" }}>
            {title ? title : 'Promotion Message'}
          </span>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Form
            form={form}
            {...formItemLayout}
            onFinish={(values) => {
              savePromotion({
                message: values?.message,
                expiry_date: values?.expiry_date || moment().add(5, "years"),
                has_expiry: values?.has_expiry
              })
            }}
            requiredMark={false}
            initialValues={initialValues}
          >
            <Form.Item 
              label="Message" 
              name="message"
              className="custom-label"
              rules={[
                {
                  required: true,
                  message: "Message required"
                },
                {
                  max: 600,
                  message: "Message must not exceed 600 character limit"
                }
              ]}  
            >
              <Input.TextArea rows={10} name="message">

              </Input.TextArea>
            </Form.Item>
            <Row gutter={12}>
              <Col span={24}>
                <Form.Item
                  label="Expiry date:"
                  name="expiry_date"
                >
                  <DatePicker 
                    format="YYYY-MM-DD HH:mm:ss"
                    showTime
                    showNow={false}
                    defaultValue={null}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Form.Item
                  name="has_expiry"
                  valuePropName="checked"
                >
                  <Checkbox name="hasExpiry">
                    This message has expiry
                  </Checkbox>
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <MyButton 
                  htmlType="submit"
                  text="SAVE"
                  loading={submitting}
                />
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
    </>
  )
}

export default PromotionMessageForm;