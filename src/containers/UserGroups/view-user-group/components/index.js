import React, { Component } from "react";
import { Row, Col, Breadcrumb } from "antd";
import PropTypes from "prop-types";
import get from "lodash/get";
import Spinner from "../../../../reusable-components/Spinner";
import { history } from "../../../../util/helpers/browserHistory";
import ProductsList from "./products-list";
import BlackListProducts from "./black-list-products";
import Filters from "./filters";
import Submit from "./submit";
import { getUserDetails } from "../../../../services/api/axiosDefaults";
import pageTitles from "../../../../util/page-titles";
import { Helmet } from "react-helmet";
import PromotionMessageForm from "./PromotionMessageForm";
import FreeProductList from "./FreeProductList";
import MyButton from "../../../../reusable-components/Button"
import AddFreeProductModal from "./AddFreeProductModal";
import EditFreeProductModal from "./EditFreeProductModal";
import moment from "moment"

export default class ViewUserGroup extends Component {
  state = {
    searchTerm: null,
    addFreeProductModalVisible: false,
    editFreeProductModalVisible: false,
    selectedEditFreeProduct: null
  };
  componentDidMount() {
    const { reset, fetchUserGroupProducts, match } = this.props;
    reset();
    fetchUserGroupProducts(get(match, "params.id"));
  }

  extractIds = (products) => {
    let ids = [];
    ids = products.reduce((acc, cur) => {
      return (acc = [...acc, cur.id]);
    }, []);
    return ids;
  };
  

  render() {
    const {
      isFetchingUserGroupProducts,
      company,
      display_name,
      blackListedProducts,
      promotion_message,
      blackList,
      isloading,
      match,
      pageTitle,
      createUserGroupPromotionMessage,
      updateUserGroupPromotionMessage,
      updatingUserGroupPromotionMessage,
      creatingUserGroupPromotionMessage,
      freeProducts,
      userGroupProducts,
      addFreeProduct,
      removeFreeProduct,
      updatingUserGroup,
      editFreeProduct
    } = this.props;

    return (
      <>
        <Helmet>
          <title>{pageTitle}</title>
        </Helmet>
        {isFetchingUserGroupProducts ? (
          <Spinner />
        ) : (
          <>
            <Row>
              <Col span={24} className="bread-crumb-container">
                {" "}
                <Breadcrumb>
                  <Breadcrumb.Item>Home</Breadcrumb.Item>
                  <Breadcrumb.Item>
                    <a
                      href="true"
                      onClick={(e) => {
                        e.preventDefault();
                        history.push("/app/user-groups");
                      }}
                    >
                      User Groups List
                    </a>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>User Group Details</Breadcrumb.Item>
                </Breadcrumb>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <h1
                  style={{
                    fontSize: "36px",
                    fontWeight: "bold",
                  }}
                >
                  {display_name || "N/A"} (
                  {(company && company.name) || "No company attached"})
                </h1>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col md={10} sm={24} xs={24}>
                <Row>
                  <Col span={24}>
                    <div style={{ marginBottom: "10px" }}>
                      <Filters
                        handleChange={(val) => this.setState({ searchTerm: val })}
                      />
                    </div>
                    <ProductsList searchTerm={this.state.searchTerm} {...this.props} />
                  </Col>
                </Row>
                <Row>
                  <Col span={24} style={{ marginTop: "50px" }}>
                    <div style={{ marginBottom: "10px" }}>
                      <b style={{ fontSize: "20px" }}>Blacklist</b>
                    </div>
                    <BlackListProducts {...this.props} />
                  </Col>
                </Row>
                <Row>
                  <Col span={24}>
                    {parseInt(get(company, "id")) ===
                    parseInt(get(getUserDetails(), "country.id")) ? (
                      <Submit
                        isloading={isloading}
                        handleSubmit={() => {
                          const blackListedids = this.extractIds(blackListedProducts);
                          blackList(get(match, "params.id"), {
                            black_list_ids: blackListedids,
                            // free_product_ids: freeProducts,
                          });
                        }}
                      />
                    ) : null}
                  </Col>
                </Row>
              </Col>
              <Col md={6} sm={24} xs={24}>
                <PromotionMessageForm
                  savePromotion={(values) => {
                    const data = {
                      message: values?.message,
                      expiry_date: moment(values?.expiry_date || "").format("YYYY-MM-DD HH:mm:ss") || moment().add(7, "years"),
                      has_expiry: values?.has_expiry,
                      user_group: Number(get(match, "params.id")),
                      company_id: company?.id
                    }
                    if(!promotion_message?.id) {
                      createUserGroupPromotionMessage(data)
                    } else {
                      updateUserGroupPromotionMessage(promotion_message?.id, data)
                    }
                  }}
                  promotionMessage={promotion_message}
                  submitting={promotion_message?.id ? updatingUserGroupPromotionMessage : creatingUserGroupPromotionMessage}
                />
              </Col>
              <Col md={8} sm={24} xs={24}>
                <Row>
                  <Col span={24}>
                    <FreeProductList
                      freeProducts={freeProducts}
                      onRemoveFreeProduct={(freeProduct) => {
                        removeFreeProduct(freeProduct)
                      }}
                      onEditFreeProduct={(freeProduct) => {
                        this.setState({...this.state, editFreeProductModalVisible: true, selectedEditFreeProduct: freeProduct})
                      }}
                    />
                  </Col>
                </Row>
                <Row style={{marginTop: 5}}>
                  <Col span={12} style={{textAlign: "right"}}>
                    <MyButton
                      text="Add new"
                      type="alt"
                      onClick={() => {
                        this.setState({...this.state, addFreeProductModalVisible: true})
                      }}
                    />
                  </Col>
                  <Col span={12}>
                    <MyButton 
                      loading={updatingUserGroup}
                      text="SAVE"
                      onClick={()=> {
                        //const blackListedIds = this.extractIds(blackListedProducts);
                        this.props.updateUserGroup(get(match, "params.id"), 
                        {
                          free_product_ids: freeProducts,
                          // black_list_ids: blackListedIds
                        })
                      }}
                    />
                  </Col>
                </Row>
                {/* <FreeProducts 
                  userGroupId={get(match, "params.id")}
                  handleUpdateUserGroup={(data) => {
                    const blackListedids = this.extractIds(blackListedProducts);
                    blackList(get(match, "params.id"), {
                            black_list_ids: blackListedids,
                            free_product_ids: data.free_product_ids,
                    });
                  }}
                  {...this.props}
                /> */}
              </Col>
            </Row>
            <AddFreeProductModal
              freeProducts={freeProducts}
              user_group_id={get(match, "params.id")}
              visible={this.state.addFreeProductModalVisible}
              onCancel={()=> {
                this.setState({...this.state, addFreeProductModalVisible: false})
              }}
              products={userGroupProducts}
              onOk={(freeProduct) => {
                addFreeProduct(freeProduct)
                this.setState({...this.state, addFreeProductModalVisible: false})
              }}
            />
            <EditFreeProductModal
              initialValues={{
                product_id: this.state.selectedEditFreeProduct?.odoo_id,
                expiry_date: moment(this.state.selectedEditFreeProduct?.expiry_date),
                min_order_value: this.state.selectedEditFreeProduct?.min_order_value || 0
              }}
              freeProducts={freeProducts}
              user_group_id={get(match, "params.id")}
              visible={this.state.editFreeProductModalVisible}
              selectedEditFreeProduct={this.state.selectedEditFreeProduct}
              onCancel={() => {
                this.setState({...this.state, editFreeProductModalVisible: false})
              }}
              products={userGroupProducts}
              onOk={(freeProduct) => {
                editFreeProduct(freeProduct)
                this.setState({...this.state, editFreeProductModalVisible: false, selectedEditFreeProduct: null})
              }}
            />
          </>
        )}
      </>
    );
  }
}

ViewUserGroup.defaultProps = {
  pageTitle: pageTitles.userGroupView
}

ViewUserGroup.propTypes = {
  fetchUserGroupProducts: PropTypes.func,
};
