import { Col, Row } from 'antd'
import React from 'react'
import FreeProductList from "./components/List"
import MyButon from "../../../../../reusable-components/Button"
import AddFreeProduct from './components/AddFreeProduct'

const FreeProducts = (props) => {
  const [ openNewFreeProductModal, setOpenNewFreeProductModal ] = React.useState(false)
  const { userGroupProducts, 
    isFetchingUserGroupProducts, 
    userGroupFreeProducts,
    addFreeProduct,
    removeFreeProduct,
  } = props

  const handleRemoveFreeProduct = (freeProduct) => {
    removeFreeProduct(freeProduct)
  }

  return (
    <>
      <Row>
        <Col span={24}>
          <span style={{fontWeight: "bold"}}>Free Products</span>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <FreeProductList
            freeProducts={userGroupFreeProducts}
            handleRemoveFreeProduct={(freeProduct) => handleRemoveFreeProduct(freeProduct)}
          />
        </Col>
      </Row>
      <Row style={{marginTop: 10}}>
        <Col span={12} style={{textAlign: "center"}}>
          <MyButon 
            onClick={(event) => {
              setOpenNewFreeProductModal(true)
            }}
            text="Add new"
            type="alt"
          />
        </Col>
        <Col span={12} style={{textAlign: "center"}}>
          <MyButon 
            onClick={(event) => {
              props.updateUserGroupPromotionMessage(props?.promotion_message?.id,{free_product_ids: userGroupFreeProducts})
            }}
            text="SAVE"
            loading={props.updatingUserGroupPromotionMessage}
          />
        </Col>
      </Row>
      <AddFreeProduct 
        userGroupId={props.userGroupId}
        userGroupProducts={userGroupProducts}
        fetchingUserGroupProducts={isFetchingUserGroupProducts}
        visible={openNewFreeProductModal}
        addFreeProduct={addFreeProduct}
        onAddFreeProduct={(freeProduct) => {
          freeProduct['user_group'] = props.userGroupId
          addFreeProduct(freeProduct)
          setOpenNewFreeProductModal(false)
        }}
        onCancel={() => {
          setOpenNewFreeProductModal(false)
        }}
      />
    </>
  )
}

export default FreeProducts