import { Modal, Row, Col, Form, Input, DatePicker, Select } from 'antd'
import React from 'react'
import moment from "moment"
import MyButton from "../../../../../../reusable-components/Button"

const formItemLayout = {
  layout: "vertical",
};


const AddFreeProduct = (props) => {
  const { visible, onOk, onCancel, userGroupProducts, fetchingUserGroupProducts, onAddFreeProduct } = props
  const [form] = Form.useForm()
  const initialValues = {
    product_id: "",
    expiry_date: "",
    min_order_value: 0
  }
  return (
    <>
      <Modal
        title="Add new free product"
        visible={visible}
        onOk={()=> onOk()}
        onCancel={()=> onCancel()}
        centered={true}
        footer={null}
        bodyStyle={{ padding: "20px 100px 70px 100px" }}
        width={630}
      >
        <Row>
          <Col md={24}>
            <Form
              form={form}
              initialValues={initialValues}
              {...formItemLayout}
              onFinish={(values) => {
                
                const item = {
                  id: values.product_id,
                  expiry_date: moment(values.expiry_date).format("YYYY-MM-DD HH:mm:ss"),
                  min_order_value: values.min_order_value,
                  user_group: props.userGroupId
                }
                onAddFreeProduct(item)
                form.resetFields()
              }}
            >
              <Form.Item
                label="Product name"
                name="product_id"
                rules={[
                  {
                    required: true,
                    message: "Please select a product"
                  }
                ]}
              >
                <Select
                  loading={fetchingUserGroupProducts}
                  showSearch
                  optionFilterProp="children"
                  filterOption={(input, option) => {
                    return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }}
                >
                  <Select.Option value="">Please select a product</Select.Option>
                  {userGroupProducts?.map(product => (
                    <Select.Option value={product.id} key={product.id}>{product.name}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                label="Till when"
                name="expiry_date"
                rules={[
                  {
                    required: true,
                    message: "Please select an expiry date"
                  }
                ]}
              > 
                <DatePicker 
                  format="YYYY-MM-DD HH:mm:ss"
                  showTime
                />
              </Form.Item>
              <Row>
                <Col span={24} style={{textAlign: "center"}}>
                  <span style={{fontWeight: "bold"}}>Condition</span>
                </Col>
              </Row>
              <Form.Item
                label="Min. Order Value"
                name="min_order_value"
              >
                <Input type="number" step="1" />
              </Form.Item>
              <MyButton 
                text="SAVE"
                htmlType="submit"
              />
            </Form>
          </Col>
        </Row>
      </Modal>
    </>
  )
}

export default AddFreeProduct