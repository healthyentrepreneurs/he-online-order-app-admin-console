import React from 'react'
import { Row, Col, Table } from "antd"
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import moment from 'moment'

const columns = (props) => [
  {
    title: "Item Name",
    dataIndex: "name",
    key: "name"
  },
  {
    title: "Till when",
    dataIndex: "expiry_date",
    key: "expiry_date",
    render: (text) => moment(text).format("YYYY-MM-DD HH:mm:ss")
  },
  {
    title: "Actions",
    dataIndex: "operations",
    key: "operations",
    render: (text, row) => (
      <Row>
        <Col span={12}>
          <EditOutlined 
            onClick={event => {
              props.onEditFreeProduct(row)
            }}
          />
        </Col>
        <Col span={12}>
          <DeleteOutlined 
            onClick={event => {
              props.onRemoveFreeProduct(row)
            }}
          />
        </Col>
      </Row>
    )
  }
]

const FreeProductList = ({
  freeProducts, 
  onRemoveFreeProduct,
  onEditFreeProduct,
}) => {
  
  return (
    <>
      <Table 
        columns={columns({ onRemoveFreeProduct, onEditFreeProduct })}
        dataSource={freeProducts}
        rowKey="id"
        pagination={false}
      />
    </>
  )
}

export default FreeProductList;