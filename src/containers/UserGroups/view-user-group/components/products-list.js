import React, { useState } from "react";
import { Row, Col, Table, Button, Tooltip } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import Spinner from "../../../../reusable-components/Spinner";
import ExpandIcon from "../../../../assets/expand-view.svg";
import MinimizeIcon from "../../../../assets/minimize-view.svg";
import get from "lodash/get";
import { getUserDetails } from "../../../../services/api/axiosDefaults";

const columns = (props) => [
  {
    title: "Item Name",
    dataIndex: "name",
    key: "name",
    width: "60%",
  },
  {
    title: "Item Code",
    dataIndex: "code",
    key: "code",
    width: "30%",
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
    align: "center",
    width: "10%",
    render: (text, row) => {
      return (
        <>
          <Button
            disabled={
              parseInt(get(props.company, "id")) ===
              parseInt(get(getUserDetails(), "country.id"))
                ? false
                : true
            }
            type="primary"
            onClick={() => props.addToBlackList(row)}
          >
            <PlusOutlined style={{ color: "white" }} />
          </Button>
        </>
      );
    },
  },
];

const List = ({
  searchTerm,
  isFetchingUserGroupProducts,
  filter,
  userGroupProducts = [],
  ...props
}) => {
  const [expandView, setExpandView] = useState(false);
  if (searchTerm) {
    userGroupProducts = userGroupProducts.filter((item) => {
      if (searchTerm && item.name && item.code)
        if (
          item.name.toLowerCase().match(searchTerm.toLowerCase()) ||
          item.name.toLowerCase().match(searchTerm.toLowerCase())
        )
          return true;
      if (searchTerm && item.name)
        if (item.name.toLowerCase().match(searchTerm.toLowerCase())) return true;
      if (searchTerm && item.code)
        if (item.code.toLowerCase().match(searchTerm.toLowerCase())) return true;
      return false;
    });
  }
  return (
    <>
      {isFetchingUserGroupProducts ? (
        <Spinner />
      ) : (
        <>
          <Row>
            <Col span={24}>
              <Tooltip
                placement="top"
                title={`${expandView ? "Click to minimize" : "Click to expand"}`}
              >
                {expandView ? (
                  <button
                    type="text"
                    className="minimize-btn"
                    onClick={(e) => {
                      e.preventDefault();
                      setExpandView(false);
                    }}
                  >
                    <img
                      src={MinimizeIcon}
                      alt="minimize icon"
                      style={{ height: "12px" }}
                    />
                  </button>
                ) : (
                  <button
                    type="text"
                    className="expand-btn"
                    onClick={(e) => {
                      e.preventDefault();
                      setExpandView(true);
                    }}
                  >
                    <img
                      src={ExpandIcon}
                      alt="expand icon"
                      style={{ height: "12px" }}
                    />
                  </button>
                )}
              </Tooltip>
              {userGroupProducts && (
                <Table
                  sticky={
                    userGroupProducts && userGroupProducts.length > 0
                      ? expandView
                        ? false
                        : true
                      : false
                  } //if no data, set sticky to false, else the header is disoriented...
                  className={`table he-table ${
                    expandView ? "expand-table" : "minimize-table"
                  }`}
                  dataSource={userGroupProducts}
                  pagination={false}
                  columns={columns(props)}
                  size="small"
                  rowKey="id"
                />
              )}
            </Col>
          </Row>
        </>
      )}
    </>
  );
};

export default List;
