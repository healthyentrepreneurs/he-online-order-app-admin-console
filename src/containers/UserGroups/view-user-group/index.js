import { connect } from "react-redux";
import { Creators } from "../../../services/redux/user_groups/actions";
import ViewUserGroup from "./components";

const mapStateToProps = (state) => {
  const {
    userGroupProducts,
    blackListedProducts,
    isloading,
    isFetchingUserGroupProducts,
    isFetchingBlackListedProducts,
    company,
    display_name,
    errors,
    promotion_message,
    updatingUserGroup,
    updateUserGroupSuccess,
    freeProducts,
    creatingUserGroupPromotionMessage,
    updatingUserGroupPromotionMessage
  } = state.user_groups;
  return {
    isloading,
    company,
    display_name,
    blackListedProducts,
    userGroupProducts,
    isFetchingUserGroupProducts,
    isFetchingBlackListedProducts,
    errors,
    promotion_message,
    updatingUserGroup,
    updateUserGroupSuccess,
    freeProducts: freeProducts,
    creatingUserGroupPromotionMessage,
    updatingUserGroupPromotionMessage
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUserGroupProducts: (id) => {
      dispatch(Creators.fetchUserGroupProducts(id));
    },
    addToBlackList: (item) => {
      dispatch(Creators.addToBlackList(item));
    },
    removeFromBlackList: (item) => {
      dispatch(Creators.removeFromBlackList(item));
    },
    blackList: (id, data) => {
      dispatch(Creators.blackList(id, data));
    },
    updateUserGroup: (id, data) => {
      dispatch(Creators.updateUserGroup(id, data))
    },
    addFreeProduct: (item) => {
      dispatch(Creators.addFreeProduct(item))
    },
    removeFreeProduct: (item) => {
      dispatch(Creators.removeFreeProduct(item))
    },
    editFreeProduct: (item) => {
      dispatch(Creators.editFreeProduct(item))
    },
    reset: () => {
      dispatch(Creators.reset());
    },
    createUserGroupPromotionMessage: (payoad) => {
      dispatch(Creators.createUserGroupPromotionMessage(payoad))
    },
    updateUserGroupPromotionMessage: (id, payload) => {
      dispatch(Creators.updateUserGroupPromotionMessage(id, payload))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewUserGroup);
