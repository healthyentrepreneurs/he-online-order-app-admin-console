import { connect } from 'react-redux';

import { Creators as PromotionCreators } from '../../services/redux/promotions/actions';
import { Creators as CompanyCreators } from "../../services/redux/country/actions"
import { Creators as UserGroupCreators } from "../../services/redux/user_groups/actions"
import { Creators as PaginationCreators } from "../../services/redux/pagination/actions"
import PromotionPage from './components';

const mapStateToProps = state => {
    return {
        fetchingPromotions: state.promotion.fetching,
        promotions: state.promotion.promotions,
        totalCount: state.promotion.totalCount,
        creatingPromotion: state.promotion.creating,
        createPromotionSuccess: state.promotion.createSuccess,
        deletingPromotion: state.promotion.deleting,
        deletePromotionSuccess: state.promotion.deleteSuccess,
        countries: state.country.countries,
        fetchingCountries: state.country.isloading,
        userGroups: state.user_groups.userGroups?.results,
        fetchingUserGroups: state.user_groups.isFetchingUserGroups,
        page: state.pagination.currentPage,
        updatingPromotion: state.promotion.updating,
        updatePromotionSuccess: state.promotion.updateSuccess,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        submit: (data) => {
            dispatch(PromotionCreators.addStaffRequest(data))
        },
        fetchPromotions: (query) => {
            dispatch(PromotionCreators.fetchPromotions(query))
        },
        fetchCountries: (query) => {
            dispatch(CompanyCreators.fetchCountries(query))
        },
        fetchUserGroups: (query) => {
            dispatch(UserGroupCreators.fetchUserGroups(query))
        },
        createPromotion: (payload) => {
            dispatch(PromotionCreators.createPromotion(payload))
        },
        updatePromotion: (id, payload) => {
            dispatch(PromotionCreators.updatePromotion(id, payload))
        },
        deletePromotion: (id) => {
            dispatch(PromotionCreators.deletePromotion(id))
        },
        modifyPagination: (obj) => {
            dispatch(PaginationCreators.modifyPagination(obj));
        },
        resetPagination: () => {
            dispatch(PaginationCreators.reset());
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PromotionPage);