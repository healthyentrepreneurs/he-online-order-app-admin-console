import { Row, Col, Input, Select, Checkbox, DatePicker } from 'antd';
import CustomButton from "../../../reusable-components/Button"
import { Form } from 'antd'
import React from 'react'
import moment from 'moment';

// const formItemLayout = {
//   labelCol: {
//     span: 24,
//   },
//   wrapperCol: {
//     span: 24,
//   },
//   layout: "vertical",
// };

const initialValues = {
  title: "",
  message: "",
  refresh: false,
  company_id: "",
  user_groups: [],
  trainings: [],
  schedule_date: "",
  created_at: ""
}

const AddPromotionForm = (props) => {

  const { countries, fetchingCountries, creatingPromotion, createPromotion, createPromotionSuccess } = props
  const [selectedCountry, setSelectedCountry] = React.useState(null)
  const [form] = Form.useForm()

  const handleSubmit = (values) => {
    const { title, message, refresh, company_id, schedule_date, trainings } = values
    // const datetime = moment(schedule_date.format("YYYY-MM-DD") + 'T' + created_at.format("HH:mm:ss a"), "YYYY-MM-DD HH:mm:ss a")
    const payload = {
      title,
      message,
      refresh,
      company_id,
      country: company_id,
      trainings: trainings,
      schedule_date: schedule_date,
      created_at: moment(),
    }
    console.log('payload', payload)
    createPromotion(payload)
  }

  React.useEffect(() => {
    if(!creatingPromotion && createPromotionSuccess) {
      form.resetFields();
    } 
    // eslint-disable-next-line
  }, [creatingPromotion])

  return (
    <>
      <Row>
        <Col span={24}>
          <span style={{fontWeight: "bold"}}>Add new promotion message</span>
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <Form
            layout="vertical"
            onFinish={handleSubmit}
            initialValues={initialValues}
            form={form}
            requiredMark={false}
          >
            <Row gutter={12}>
              <Col md={6} sm={24} xs={24}>
                <Form.Item
                  label="Title:"
                  className="custom-label"
                  name="title"
                  rules={[
                    {
                      required: true,
                      message: "Title is required"
                    },
                    {
                      max: 100,
                      message: "You exceeded 100 character limit"
                    }
                  ]}
                >
                  <Input.TextArea 
                    autoSize={{
                      minRows: 6
                    }}
                  />
                </Form.Item>
              </Col>
              <Col md={6} sm={24} xs={24}>
                <Form.Item
                  label="Message:"
                  className="custom-label"
                  name="message"
                  rules={[
                    {
                      required: true,
                      message: "Message is required"
                    },
                    {
                      max: 600,
                      message: "You exceeded 600 character limit"
                    }
                  ]}
                >
                  <Input.TextArea 
                    autoSize={{
                      minRows: 6
                    }}
                  />
                </Form.Item>
              </Col>
              <Col md={6} sm={24} xs={24}>
                <Row>
                  <Col span={24}>
                    <Form.Item
                      label="Send to:"
                      className="custom-label"
                      name="company_id"
                      rules={[
                        {
                          required: true,
                          message: "Country is required"
                        }
                      ]}
                    >
                      <Select
                        loading={fetchingCountries}
                        onChange={(value)=> {
                          const country = countries?.filter(item=> item?.id === value)[0];
                          setSelectedCountry(country)
                          form.resetFields(["trainings"])
                        }}
                      >
                        <Select.Option value="" key="default-country-id">Please select a country</Select.Option>
                        {countries && countries.map(country => (
                          <Select.Option value={country?.id} key={country?.id}>{country?.name}</Select.Option>
                        ))}
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col span={24}>
                    <Form.Item
                      name="trainings"
                      rules={[
                        {required: true, message: "Training is required"}
                      ]}
                    >
                      <Checkbox.Group>
                        {selectedCountry && form.getFieldValue("company_id") && selectedCountry.trainings?.map(training => (
                          <Row key={training?.id}>
                            <Col span={24}>
                              <Checkbox key={`user-group-checkbox-${training.id}`} value={training.id}>
                                {training.name}
                              </Checkbox>
                            </Col>
                          </Row>
                        ))}
                      </Checkbox.Group>
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col md={6} sm={24} xs={24}>
                <Row>
                  <Col span={24}>
                    <Row gutter={12}>
                      <Col span={24}>
                        <Form.Item
                          label="Send when:"
                          className="custom-label"
                          name="schedule_date"
                          rules={[
                            {
                              required: true,
                              message: "Date is required"
                            }
                          ]}
                        >
                          <DatePicker 
                            format="YYYY-MM-DD HH:mm:ss"
                            showTime={true}
                            showNow={false}
                          />
                        </Form.Item>
                      </Col>
                      {/* <Col span={12}>
                        <Form.Item
                          label=" "
                          className="custom-label"
                          name="created_at"
                          rules={[
                            {
                              required: true,
                              message: "Time is required"
                            }
                          ]}
                        >
                          <TimePicker 
                            format="HH:mm"
                            use12Hours
                            showNow
                          />
                        </Form.Item>
                      </Col> */}
                    </Row>
                  </Col>
                </Row>
                <Row>
                  <Col span={24}>
                    <Form.Item
                      name="refresh"
                      valuePropName="checked"
                      wrapperCol={{ span: 24 }}
                    >
                      <Checkbox>
                        Refresh product list upon opening notification
                      </Checkbox>
                    </Form.Item>
                  </Col>
                </Row>
                <Row style={{marginTop: 5}}>
                  <Col span={24}>
                    <CustomButton 
                      htmlType="submit" 
                      type="primary" 
                      text="SAVE" 
                      loading={creatingPromotion}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
    </>
  )
}

export default AddPromotionForm