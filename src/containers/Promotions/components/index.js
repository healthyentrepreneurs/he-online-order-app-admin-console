import { Row, Col, Form, Checkbox, Select, Modal, DatePicker, Input } from 'antd'
import React from 'react'
import AddPromotionForm from './AddPromotionForm'
import PromotionList from './PromotionList'
import { Helmet } from "react-helmet"
import pageTitles from '../../../util/page-titles'
//import EditPromotion from './EditPromotion'
import MySwal from '../../../util/sweet-alert'
import moment from 'moment'
import MyButton from "../../../reusable-components/Button"



const PromotionPage = (props) => {
  //props
  const { 
    fetchCountries,
    fetchPromotions,
    page,
    resetPagination,
    deletePromotion,
    deletingPromotion,
    deletePromotionSuccess,
    countries
  } = props

  //local states
  const [selectedPromotion, setSelectedPromotion] = React.useState(null)
  const [editPromotionModalVisible, setEditPromotionModalVisible] = React.useState(false)
  const [editSelectedCountry, setEditSelectedCountry] = React.useState(null)
  const [editForm] = Form.useForm()

  const handleDeletePromotion = (promo) => {
    MySwal.fire({
      title: `Are you sure you want to delete promotion '${promo?.title}'`,
      showDenyButton: true,
      denyButtonText: "No, do not delete",
      confirmButtonText: "Yes delete",
      confirmButtonColor: "#30994b",
    }).then(result => {
      if(result.isConfirmed) {
        deletePromotion(promo.id)
      }
    })
  }

  const handleEditPromotion = (promo) => {
    setEditPromotionModalVisible(true)
    setSelectedPromotion(promo)
    const country = countries?.find(country => country.id === promo.company_id)
    setEditSelectedCountry(country)
    editForm.setFieldsValue({
      title: promo?.title,
      message: promo?.message,
      refresh: promo?.refresh || false,
      company_id: promo?.company_id,
      trainings: promo?.trainings || [],
      schedule_date: moment(promo?.schedule_date),
    })

  }

  React.useEffect(() => {
    resetPagination()
    fetchCountries()
    fetchPromotions({
      page: page
    })
    // eslint-disable-next-line
  }, [])
  

  React.useEffect(()=> {
    if(!deletingPromotion && deletePromotionSuccess) {
      setSelectedPromotion(null)
    }
  }, [deletingPromotion, deletePromotionSuccess])

  React.useEffect(() => {
    if(deletingPromotion) {
      MySwal.showLoading()
    }
  }, [deletingPromotion])

  React.useEffect(() => {
    if(props.updatePromotionSuccess && !props.updatingPromotion) {
      setSelectedPromotion(null)
      setEditPromotionModalVisible(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.updatingPromotion])

  return (
    <>
      <Helmet>
        <title>{pageTitles.promotions}</title>
      </Helmet>
      <Row style={{marginBottom: 5}}>
        <Col span={24}>
          <h1 className="header">Promotions</h1>
        </Col>
      </Row>
      <Row style={{marginBottom: 10}}>
        <Col span={24}>
          <AddPromotionForm 
            {...props}
          />
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <PromotionList
            handleEditPromotion={handleEditPromotion}
            handleDeletePromotion={handleDeletePromotion}
            {...props}
          />
        </Col>
      </Row>
      {/* <EditPromotion
        selectedCountryId = {selectedCountryId}
        initialValues={{
          title: selectedPromotion?.title,
          message: selectedPromotion?.message,
          refresh: selectedPromotion?.refresh || false,
          company_id: selectedPromotion?.company_id,
          user_groups: selectedPromotion?.user_groups || [],
          schedule_date: moment(selectedPromotion?.schedule_date),
          created_at: "22:10:01",
        }}
        loading={props.updatingPromotion}
        visible={editPromotionModalVisible}
        promotion={selectedPromotion}
        onSubmit={(promotion) => {
          console.log('edit', promotion)
          props.updatePromotion(selectedPromotion?.id, promotion)
        }}
        onCancel={()=> {
          console.log('close')
          setEditPromotionModalVisible(false)
          setSelectedPromotion(null)
        }}
        {...props}
      /> */}
      <Modal
        visible={editPromotionModalVisible}
        maskClosable={false}
        title="Edit Promotion"
        footer={null}
        onCancel={()=> {
          setEditPromotionModalVisible(false)
          setSelectedPromotion(null)
        }}
        forceRender
      >
        <Row>
          <Col span={24}>
            <span style={{fontWeight: "bold"}}>Edit promotion message</span>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Form
              form={editForm}
              layout="vertical"
              onFinish={(values) => {
                props.updatePromotion(selectedPromotion?.id, values)
              }}
              // initialValues={{
              //   title: selectedPromotion?.title,
              //   message: selectedPromotion?.message,
              //   refresh: selectedPromotion?.refresh || false,
              //   company_id: selectedPromotion?.company_id,
              //   user_groups: selectedPromotion?.user_groups || [],
              //   schedule_date: moment(selectedPromotion?.schedule_date),
              //   created_at: "22:10:01",
              // }}
              requiredMark={false}
            >
              <Form.Item
                label="Title"
                name="title"
                rules={[
                  {
                    required: true,
                    message: "Title is required"
                  },
                  {
                    max: 100,
                    message: "You exceeded 100 character limit"
                  }
                ]}
              >
                <Input.TextArea type="text" rows={5} />
              </Form.Item>
              <Form.Item
                label="Message"
                name="message"
                rules={[
                  {
                    required: true,
                    message: "Message is required"
                  },
                  {
                    max: 600,
                    message: "Message exceeded 600 character limit!"
                  }
                ]}
              >
                <Input.TextArea rows={5} />
              </Form.Item>
              <Form.Item
                label="Send to:"
                className="custom-label"
                name="company_id"
                rules={[
                  {
                    required: true,
                    message: "Company is required"
                  }
                ]}
              >
                <Select
                  loading={props.fetchingCountries}
                  onChange={(value)=> {
                    const country = countries?.find(item=> item?.id === value);
                    setEditSelectedCountry(country)
                    // editForm.resetFields({
                    //   user_groups: []
                    // })
                  }}
                >
                  <Select.Option value="" key="default-country-id">Please select a country</Select.Option>
                  {countries && countries.map(country => (
                    <Select.Option value={country?.id} key={country?.id}>{country?.name}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                name="trainings"
              >
                <Checkbox.Group>
                  {editSelectedCountry?.trainings?.map(training => (
                    <Row key={training?.id}>
                      <Col span={24}>
                        <Checkbox key={`user-group-checkbox-${training.id}`} value={training.id}>
                          {training.name}
                        </Checkbox>
                      </Col>
                    </Row>
                  ))}
                </Checkbox.Group>
              </Form.Item>
              <Form.Item>
                <Form.Item
                  label="Send when:"
                  className="custom-label"
                  name="schedule_date"
                  rules={[
                    {
                      required: true,
                      message: "Date and time is required"
                    }
                  ]}
                >
                  <DatePicker
                    format="YYYY-MM-DD HH:mm:ss"
                    showTime={true}
                    showNow={false}
                  />
                </Form.Item>
              </Form.Item>
              <Form.Item
                name="refresh"
                valuePropName="checked"
                wrapperCol={{ span: 24 }}
              >
                <Checkbox>
                  Refresh product list upon opening notification
                </Checkbox>
              </Form.Item>
              <MyButton 
                htmlType="submit"
                text="UPDATE"
                loading={props.updatingPromotion}
              />
            </Form>
          </Col>
        </Row>
      </Modal>
    </>
  )
}

export default PromotionPage