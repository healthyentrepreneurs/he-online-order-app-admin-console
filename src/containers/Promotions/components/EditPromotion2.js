import { Modal, Row, Col, Form, Input, Select, Checkbox , DatePicker } from 'antd'
import React from 'react'
import MyButton from "../../../reusable-components/Button"

const formItemLayout = {
  layout: "vertical",
};

const EditPromotion = (props) => {
  const { visible, onCancel, promotion, countries, fetchingCountries, initialValues, onSubmit, selectedCountryId } = props
  const [selectedCountry, setSelectedCountry] = React.useState(null)

  const [form] = Form.useForm()

  // const initialValues = {
  //   title: promotion?.title,
  //   message: promotion?.message,
  //   refresh: promotion?.refresh,
  //   company_id: promotion?.company_id,
  //   user_group: (promotion?.user_groups || [])[0],
  //   schedule_date: promotion?.schedule_date,
  //   created_at: promotion?.created_at
  // }

  React.useEffect(()=> {
    form.resetFields()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialValues])
  

  React.useEffect(() => {
    const country = countries?.filter(item=> item?.id === selectedCountryId)[0];
    
    setSelectedCountry(country)
    //form.resetFields(["user_groups"])
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedCountryId])

  return (
    <>
      <Modal
        visible={visible}
        maskClosable={false}
        title="Edit Promotion"
        footer={null}
        forceRender
        onCancel={()=> {
          onCancel()
        }}
      >
        <Row>
          <Col span={24}>
            <span style={{fontWeight: "bold"}}>Edit promotion message</span>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <Form
              form={form}
              {...formItemLayout}
              onFinish={(values) => {
                onSubmit(values)
              }}
              initialValues={initialValues}
              requiredMark={false}
            >
              <Form.Item
                label="Title"
                name="title"
                rules={[
                  {
                    required: true,
                    message: "Title is required"
                  }
                ]}
              >
                <Input.TextArea type="text" rows={5} />
              </Form.Item>
              <Form.Item
                label="Message"
                name="message"
                rules={[
                  {
                    required: true,
                    message: "Message is required"
                  },
                  {
                    max: 600,
                    message: "Message exceeded 600 character limit!"
                  }
                ]}
              >
                <Input.TextArea rows={5} />
              </Form.Item>
              <Form.Item
                label="Send to:"
                className="custom-label"
                name="company_id"
                rules={[
                  {
                    required: true,
                    message: "Company is required"
                  }
                ]}
              >
                <Select
                  loading={fetchingCountries}
                  onChange={(value)=> {
                    const country = countries?.filter(item=> item?.id === value)[0];
                    setSelectedCountry(country)
                    form.resetFields(["user_groups"])
                  }}
                >
                  <Select.Option value="" key="default-country-id">Please select a country</Select.Option>
                  {countries && countries.map(country => (
                    <Select.Option value={country?.id} key={country?.id}>{country?.name}</Select.Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                name="user_groups"
              >
                <Checkbox.Group>
                  {selectedCountry && selectedCountry?.user_groups?.map(userGroup => (
                    <Row key={userGroup?.id}>
                      <Col span={24}>
                        <Checkbox key={`user-group-checkbox-${userGroup.id}`} value={userGroup.id}>
                          {userGroup.name}
                        </Checkbox>
                      </Col>
                    </Row>
                  ))}
                </Checkbox.Group>
              </Form.Item>
              <Form.Item>
                <Form.Item
                  label="Send when:"
                  className="custom-label"
                  name="schedule_date"
                  rules={[
                    {
                      required: true,
                      message: "Date and time is required"
                    }
                  ]}
                >
                  <DatePicker 
                    format="YYYY-MM-DD HH:mm:ss"
                    showTime={true}
                  />
                </Form.Item>
              </Form.Item>
              <Form.Item
                name="refresh"
                valuePropName="checked"
                wrapperCol={{ span: 24 }}
              >
                <Checkbox>
                  Refresh product list upon opening notification
                </Checkbox>
              </Form.Item>
              <MyButton 
                htmlType="submit"
                text="UPDATE"
                loading={props.loading}
              />
            </Form>
          </Col>
        </Row>
      </Modal>
    </>
  )
}

export default EditPromotion