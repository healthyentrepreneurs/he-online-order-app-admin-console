import { Col, Row, Table, Button, Spin } from 'antd'
import React from 'react'
import Pagination from '../../../reusable-components/Pagination'
import DeleteIcon from "../../../assets/delete.svg";
import { PROMOTIONS_LIMIT } from '../../../services/api/promotions';
import { EditOutlined } from "@ant-design/icons"
import moment from "moment"

const colums = (props) => [
  {
    title: "Date/Time",
    dataIndex: "schedule_date",
    key: "schedule_date",
    render: (text) => moment(text).format("YYYY-MM-DD HH:mm:ss")
  },
  {
    title: "Title",
    dataIndex: "title",
    key: "title"
  },
  {
    title: "Message",
    dataIndex: "message",
    key: "message"
  },
  {
    title: "Country",
    dataIndex: "country",
    key: "country",
    render: (text, row) => row?.country?.name
  },
  {
    title: "",
    dataIndex: "operations",
    key: "operations",
    render: (text, row) => (
      <Row gutter={12}>
        <Col span={12}>
          <EditOutlined 
            style={{ color: "#A9B4C1", paddingRight: "10px" }}
            className="edit-icon"
            onClick={() => props.handleEditPromotion(row)}
            disabled={props.fetchingCountries}
          />
        </Col>
        <Col span={12}>
          <Button
              type="text"
              style={{ padding: 0, height: 0 }}
              onClick={()=> props.handleDeletePromotion(row)}
              loading={props.deleting && props.promotion?.id === row.id}
            >
              <img src={DeleteIcon} alt="delete icon" />
            </Button>
        </Col>
      </Row>
    )
  }
]


const PromotionList = (props) => {
  
  const {
    promotions,
    fetchPromotions,
    fetchingPromotions,
    page,
    totalCount,
    title,
    modifyPagination,
    deletingPromotion,
    selectedPromotion,
    fetchingCountries
  } = props

  return (
    <>
      <Row>
        <Col span={24}>
          <span style={{fontWeight: "bold"}}>{title || 'Scheduled'}</span>
        </Col>
      </Row>
      {fetchingPromotions ? <Spin /> : 
        (
          <>
            <Row style={{marginBottom: 5}}>
              <Col span={24}>
                <Table 
                  columns={colums({
                    handleDeletePromotion: (row)=> props.handleDeletePromotion(row),
                    handleEditPromotion: (row) => props.handleEditPromotion(row),
                    deleting: deletingPromotion,
                    promotion: selectedPromotion,
                    fetchingCountries: fetchingCountries,
                  })
                  }
                  dataSource={promotions}
                  rowKey="id"
                  pagination={false}
                />
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Pagination 
                  handlePagination={(page) => {
                    fetchPromotions({
                      page
                    })
                  }}
                  page={page}
                  totalCount={totalCount}
                  defaultPageSize={PROMOTIONS_LIMIT}
                  modifyPagination={modifyPagination}
                />
              </Col>
            </Row>
          </>
        )
      }
    </>
  )
}

export default PromotionList