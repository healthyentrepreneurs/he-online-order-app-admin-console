import React, { useEffect } from "react";
import { Row, Col, Table } from "antd";
import { EyeOutlined } from "@ant-design/icons";
import Spinner from "../../../../reusable-components/Spinner";
import { uppercaseFirstLetter } from "../../../../util/helpers/reusable-functions";
import moment from "moment";
import { Helmet } from "react-helmet";
import pageTitles from "../../../../util/page-titles";

const checkMethod = (method) => {
  if (method === "momo") method = "MTN";
  return method;
};

const columns = (props) => [
  {
    title: "Date Created",
    dataIndex: "created_at",
    key: "created_at",
    width: "20%",
    render: (text, row) => {
      return moment(row.created_at).format("YYYY-MM-DD");
    },
  },
  {
    title: "Company",
    dataIndex: "company_name",
    key: "company_name",
    width: "40%",
  },
  {
    title: "Country",
    dataIndex: "name",
    key: "name",
    width: "40%",
  },
  {
    title: "Payment Methods",
    dataIndex: "payment_methods",
    key: "payment_methods",
    width: "20%",
    render: (text, row) => {
      return row.payment_methods && row.payment_methods.length > 0 ? (
        <span className="item-table-details">
          {row.payment_methods.reduce((acc, cur) => {
            return (acc = acc
              ? acc + ", " + uppercaseFirstLetter(checkMethod(cur.name))
              : uppercaseFirstLetter(checkMethod(cur.name)));
          }, "")}
        </span>
      ) : (
        "N/A"
      );
    },
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
    align: "center",
    render: (text, row) => {
      return (
        <>
          <EyeOutlined
            onClick={() => {
              props.chooseCompany(row);
              props.fetchPricelistsByCountry(row);
              props.openModal();
            }}
            style={{ color: "#A9B4C1" }}
            className="edit-icon"
          />
        </>
      );
    },
  },
];

const List = ({ countries, isloading, sync, ...props }) => {
  const { pageTitle } = props
  useEffect(() => {
    if (sync) props.fetchCountries({ update: true });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sync]);
  return (
    <>
      <Helmet>
        <title>{pageTitle}</title>
      </Helmet>
      {isloading ? (
        <Spinner />
      ) : (
        <>
          <Row>
            <Col span={24}>
              {countries && (
                <Table
                  className="table he-table"
                  dataSource={countries}
                  pagination={false}
                  columns={columns(props)}
                  rowKey="id"
                />
              )}
            </Col>
          </Row>
        </>
      )}
    </>
  );
};

List.defaultProps = {
  pageTitle: pageTitles.companyList
}

export default List;
