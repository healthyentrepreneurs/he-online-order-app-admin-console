import React, { Component } from "react";
import Header from "./header";
import List from "./list";
import EditModal from "./edit-modal";

import PropTypes from "prop-types";

import "../../../../styles/modals.less";

export default class ListComponent extends Component {
  state = {
    visible: false,
  };

  async componentDidMount() {
    const { countries, fetchPaymentMethods, fetchCountries, fetchPaymentTerms } = this.props;
    
    // check local state if empty, then fetch, otherwise one will need to click on the sync button to sync this data.
    if (countries?.length === 0) {
      fetchCountries();
    }
    fetchPaymentMethods();
    fetchPaymentTerms(null);
  }

  componentDidUpdate(prevProps) {
    const { isUpdated, fetchCountries } = this.props;
    console.log('PRICE LIST IN COMPONENTE', this.props.pricelists)
    if (isUpdated !== prevProps.isUpdated) {
      if (isUpdated) {
        this.setState({ visible: false });
        fetchCountries();
      }
    }
  }

  render() {
    return (
      <>
        <Header
          handleChange={(fromDate, endDate, status, searchTerm) => {}}
          {...this.props}
        />
        <div className="filters-list">
          <List openModal={() => this.setState({ visible: true })} {...this.props} />
        </div>
        {this.state.visible && (
          <EditModal
            visible={this.state.visible}
            company={this.props.company}
            all_payment_methods={this.props.payment_methods}
            all_payment_terms={this.props.payment_terms}
            handleCancel={() => this.setState({ visible: false })}
            {...this.props}
          />
        )}
      </>
    );
  }
}

ListComponent.propTypes = {
  isloading: PropTypes.bool,
};
