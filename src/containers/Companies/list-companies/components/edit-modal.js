import React, { useEffect, useState } from "react";
import { Row, Col, Modal, Form, Input, Checkbox, Select } from "antd";
import Spinner from "../../../../reusable-components/Spinner";
import { uppercaseFirstLetter } from "../../../../util/helpers/reusable-functions";
import { LANGUAGES } from "../../../../util/configs";

const formItemLayout = {
  labelCol: {
    span: 24,
  },
  wrapperCol: {
    span: 24,
  },
  layout: "vertical",
};

const checkMethod = (method) => {
  if (method === "momo") method = "MTN";
  return method;
};

// const checkTerm = (term) => {
//   if (term === "momo") term = "MTN";
//   return term;
// }

const EditModal = ({
  company,
  all_payment_methods,
  all_payment_terms,
  visible,
  isEditting,
  fetchPricelistsByCountry,
  pricelists,
  loading_pricelists,
  ...props
}) => {

  const [checkedList, setCheckedList] = useState(
    company && company.payment_methods && company.payment_methods.length > 0
      ? company.payment_methods.reduce((acc, cur) => {
        return (acc = [...acc, cur.id]);
      }, [])
      : []
  );

  const [termCheckedList, setTermCheckedList] = useState([])

  // const [termCheckedList, setTermCheckedList] = useState(
  //   company && company.payment_terms && company.payment_terms.length > 0
  //     ? company.payment_terms.reduce((acc, cur) => {
  //       return (acc = [...acc, cur]);
  //     }, [])
  //     : []
  // );


  const onChange = (list) => {
    setCheckedList(list);
  };

  const onTermChange = (list) => {
    console.log('------> selected payment terms', list)
    setTermCheckedList(list);
  };
  let buttonState = isEditting ? (
    <Spinner color="#ffffff" />
  ) : (
    "Update Company Information"
  );

  useEffect(() => {
    if(company?.id) {
      fetchPricelistsByCountry(company)
    }
    
  }, [company, fetchPricelistsByCountry])

  useEffect(() => {
    console.log('payment terms...', company?.payment_terms)
    let arrList = Array.isArray(company?.payment_terms) ? company?.payment_terms || [] : []
    if(company?.blank_payment_term) {
      arrList.push('blank')
      setTermCheckedList(arrList)
    } else {
      console.log('-----> arr', arrList)
      setTermCheckedList(arrList?.filter(it => !it.blank) || [])
    }
    // eslint-disable-next-line 
  }, [company?.payment_terms])


  return (
    <>
      <Modal
        title="Edit Company"
        visible={visible}
        onOk={() => props.handleOk()}
        onCancel={() => props.handleCancel()}
        centered={true}
        footer={null}
        bodyStyle={{ padding: "20px 100px 70px 100px" }}
        width={630}
      >
        <Form
          initialValues={{
            name: company && company.name,
            company_name: company && company.company_name,
            currency: company && company.currency,
            phone_number_prefix: company && company.phone_number_prefix,
            payment_methods: company && company.payment_methods,
            payment_terms: company && company.payment_terms,
            minimum_threshold: company && company.minimum_threshold,
            minimum_order_value: company?.minimum_order_value,
            language_code: company?.language_code,
            pricelist: company?.pricelist_id,
          }}
          onFinish={(values) => {
            console.log('lets check termlist', termCheckedList)
            //const myPaymentTerms = termCheckedList.filter(it => it !== 'blank')
            const payload = {
              payment_method_ids: checkedList,
              minimum_order_value: values.minimum_order_value,
              minimum_threshold: values.minimum_threshold,
              phone_number_prefix: values.phone_number_prefix,
              payment_terms: termCheckedList?.filter(it => it !== 'blank'),
              language_code: values?.language_code,
              pricelist_id: values?.pricelist,
              blank_payment_term: (termCheckedList?.filter(it => it === 'blank') || []).length > 0
              
            }
            console.log('values', payload)
            // props.updateCountry(company.id, {
            //   payment_method_ids: checkedList,
            //   minimum_order_value: values.minimum_order_value,
            //   minimum_threshold: values.minimum_threshold,
            //   phone_number_prefix: values.phone_number_prefix,
            //   payment_terms: termCheckedList,
            // });
            props.updateCountry(company.id, payload);
          }}
          {...formItemLayout}
          autoComplete="off"
        >
          <Row>
            <Col span={24}>
              <Row>
                <Col span={24}>
                  <Form.Item
                    label="Company"
                    name="company_name"
                    className="custom-label"
                  >
                    <Input size="large" className="form-input" disabled />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item label="Country" name="name" className="custom-label">
                    <Input size="large" className="form-input" disabled />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item label="Currency" name="currency" className="custom-label">
                    <Input size="large" className="form-input" disabled />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item label="Default Price List" name="pricelist">
                    <Select size="large" style={{ width: '100%' }} loading={loading_pricelists}>
                      {(pricelists || []).map(pl => (
                        <Select.Option style={{ width: '100%' }} value={pl.id} key={pl.id}>{pl.name}</Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item label="Min. stock threshold" name="minimum_threshold" className="custom-label">
                    <Input size="large" className="form-input" />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item label="Min. order value" name="minimum_order_value" className="custom-label">
                    <Input size="large" className="form-input" />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item label="Language" name="language_code">
                    <Select size="large" style={{ width: '100%' }}>
                      {LANGUAGES.map(lang => (
                        <Select.Option style={{ width: '100%' }} value={lang.id} key={lang.id}>{lang.label}</Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item label="Phone nr. prefix" name="phone_number_prefix" className="custome-label">
                    <Input size="large" className="form-input" />
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item label="Payment terms for credit" className="custom-label">
                    {all_payment_terms && all_payment_terms.length > 0 ? (
                      <Checkbox.Group value={termCheckedList} onChange={onTermChange}>
                        <Row>
                          {all_payment_terms.map((term) => (
                            <Col span={24} key={term.id}>
                              <Checkbox key={term.id} value={term.id}>
                                {uppercaseFirstLetter(term?.name)}
                              </Checkbox>
                            </Col>
                          ))}
                          <Checkbox key="blank" value="blank">
                            Blank
                          </Checkbox>
                        </Row>
                      </Checkbox.Group>
                    ) : (
                      "N/A"
                    )}
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col span={24}>
                  <Form.Item label="Payment Methods" className="custom-label">
                    {all_payment_methods && all_payment_methods.length > 0 ? (
                      <Checkbox.Group value={checkedList} onChange={onChange}>
                        <Row>
                          {all_payment_methods.map((method) => (
                            <Col span={24} key={method.id}>
                              <Checkbox key={method.id} value={method.id}>
                                {uppercaseFirstLetter(checkMethod(method.name))}
                              </Checkbox>
                            </Col>
                          ))}
                        </Row>
                      </Checkbox.Group>
                    ) : (
                      "N/A"
                    )}
                  </Form.Item>
                </Col>
              </Row>
              <Row>
                <Col
                  span={24}
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <button type="submit" className="custom-modal-btn">
                    {buttonState}
                  </button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
};

export default EditModal;
