import React from "react";

const Header = ({ ...props }) => {
  return (
    <>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <h1 className="header">Companies</h1>
        <button className="header-btn" onClick={() => props.toggleSync()}>
          Sync
        </button>
      </div>
    </>
  );
};

export default Header;
