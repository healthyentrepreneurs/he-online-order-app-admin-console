import { connect } from "react-redux";
import { Creators } from "../../../services/redux/country/actions";
import { Creators as PaymentMethodsCreators } from "../../../services/redux/payment_methods/actions";
import { Creators as AccountingCreators } from "../../../services/redux/accounting/actions"
import { Creators as PricelistCreators } from "../../../services/redux/pricelist/actions"
import ListComponent from "./components";

const mapStateToProps = (state) => {
  const {
    countries,
    isloading,
    sync,
    company,
    isEditting,
    isUpdated,
  } = state.country;
  return {
    countries,
    isloading,
    sync,
    company,
    payment_methods: state.payment_methods.payment_methods,
    isEditting,
    isUpdated,
    payment_terms: state.accounting.payment_terms,
    loading_payment_terms: state.accounting.isloading,
    pricelists: state.pricelist.pricelists,
    loading_pricelists: state.pricelist?.is_loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCountries: (query) => {
      dispatch(Creators.fetchCountries(query));
    },
    fetchPaymentMethods: () => {
      dispatch(PaymentMethodsCreators.fetchPaymentMethods());
    },
    chooseCompany: (company) => {
      dispatch(Creators.chooseCompany(company));
    },
    updateCountry: (id, data) => {
      dispatch(Creators.updateCountry(id, data));
    },
    toggleSync: () => {
      dispatch(Creators.toggleSync());
    },
    fetchPaymentTerms: query => dispatch(AccountingCreators.fetchPaymentTerms(query)),
    resetPaymentTerms: () => dispatch(AccountingCreators.resetPaymentTerms()),
    fetchPricelistsByCountry: country => dispatch(PricelistCreators.fetchPricelistsByCountry(country))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListComponent);
