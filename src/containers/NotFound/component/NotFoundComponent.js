import React from "react";
import "../../../styles/notfound.less";
import { Redirect } from "react-router-dom";

const NotFound = (props) => {
	return <Redirect to={{ pathname: "/" }} />;
	// return <div>Not found route</div>;
};

export default NotFound;
