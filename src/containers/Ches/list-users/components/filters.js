import React from "react";
import { Input, Select, Row, Col } from "antd";
// import Button from "../../../../reusable-components/Button"
import { EyeOutlined, SyncOutlined } from "@ant-design/icons";
import LastSyncStatus from "./last-sync-status";
import HeButton from "../../../../reusable-components/HEButton";

const { Option } = Select;

const Filters = ({
  isloading, countries, handleChange, districts, clusters,
  syncUsers, refreshing, onSyncClick,lastSyncTime, lastSyncData, lastSyncStatus, onDisplaySyncError,
  ...props
}) => {

  const filter = props?.filter

  const onChange = (e) => {
    const val = e.target.value;
    //setSearchTerm(val);
    const query = {
      ...filter,
      search_term: val
    }
    props.setFilter(query)
    handleChange(query);
  };

  const handleOnChange = (query) => {
    props.setFilter(query)
    handleChange(query)
  }

  return (
    <>
      <div>
        <Row gutter={6} >
          <Col sm={24} md={9}>
            <Row gutter={12}>
              <Col md={5} sm={12}>
                <h1 className="header">Users</h1>
              </Col>
              <Col offset={1} md={18} sm={12}>
                <Row>
                  <Col span={4}>
                    <HeButton
                      text={props.generatePasswordLoading ? "downloading..." : "CSV"}
                      onClick={(event) => props.handleDownloadPasswords(event, "XLS")}
                      disabled={props.generatePasswordLoading}
                    />
                  </Col>
                  <Col span={4} style={{ paddingLeft: 5 }}>
                    <HeButton
                      disabled={refreshing}
                      text={<SyncOutlined spin={refreshing} />}
                      onClick={(event) => {
                        onSyncClick({
                          limit: 100,
                          status: filter?.status,
                          country: filter?.company_id,
                          cluster: filter?.cluster?.id,
                          district: filter?.district_id,
                          searchTerm: filter?.search_term
                        }
                        )
                      }
                      }
                    />
                  </Col>
                  <Col span={12}>
                    <LastSyncStatus 
                      lastSyncTime={lastSyncTime} 
                      lastSyncData={lastSyncData} 
                      lastSyncStatus={lastSyncStatus}
                      onDisplayError={onDisplaySyncError}
                    />
                  </Col>
                  <Col span={4}>
                    <HeButton 
                      onClick={() => onDisplaySyncError()}
                      text={<EyeOutlined/>}
                      type="alt"
                      disabled={!lastSyncData}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col offset={1} md={14} sm={12}>
            <Row gutter={12} style={{ height: '100%', alignItems: 'center' }}>
              <Col
                md={2}
                sm={12}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <h4 style={{ padding: "0", margin: "0" }}>Filter</h4>
              </Col>
              <Col md={3}>
                <Select
                  value={filter?.company_id}
                  allowClear={true}
                  name="company_id"
                  size="large"
                  style={{ width: "100%" }}
                  loading={props.countriesLoading}
                  showSearch
                  placeholder="company"
                  optionFilterProp="children"
                  onChange={(value) => {
                    handleOnChange({
                      ...filter,
                      company_id: value,
                      district_id: undefined,
                      cluster_id: undefined
                    })
                  }}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {countries && countries.length > 0 ? (
                    countries.map((country) => (
                      <Option key={country?.id} value={country?.id}>
                        {country?.company_name}
                      </Option>
                    ))
                  ) : (
                    <Option disabled value="N/A">
                      Select country
                    </Option>
                  )}
                </Select>
              </Col>
              <Col md={3}>
                <Select
                  dropdownMatchSelectWidth={false}
                  allowClear={true}
                  name="cluster"
                  size="large"
                  style={{ width: "100%" }}
                  loading={props.clustersLoading}
                  showSearch
                  placeholder="cluster"
                  optionFilterProp="children"
                  value={filter?.cluster_id}
                  onChange={(value) => {
                    handleOnChange({
                      ...filter,
                      cluster_id: value
                    });
                  }}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {clusters && clusters.length > 0 ? (
                    clusters.map((item) => (
                      <Option key={item.id} value={item.name}>
                        {item.name}
                      </Option>
                    ))
                  ) : (
                    <Option disabled value="N/A">
                      Select cluster
                    </Option>
                  )}
                </Select>
              </Col>
              <Col md={3}>
                <Select
                  dropdownMatchSelectWidth={false}
                  allowClear={true}
                  name="district"
                  size="large"
                  style={{ width: "100%" }}
                  loading={props.districtsLoading}
                  showSearch
                  placeholder="district"
                  optionFilterProp="children"
                  value={filter?.district_id}
                  onChange={(value) => {
                    handleOnChange({
                      ...filter,
                      district_id: value
                    });
                  }}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {districts && districts.length > 0 ? (
                    districts.filter(item => {
                      if (filter?.cluster_id) {
                        return item.country_id === filter?.country?.id
                      } else {
                        return item
                      }
                    })
                      .map((item) => (
                        <Option key={item.id} value={item.name}>
                          {item.name}
                        </Option>
                      ))
                  ) : (
                    <Option disabled value="N/A">
                      Select district
                    </Option>
                  )}
                </Select>
              </Col>
              <Col md={3}>
                <Select
                  allowClear={true}
                  name="status"
                  value={filter?.status}
                  size="large"
                  style={{ width: "100%" }}
                  showSearch
                  placeholder="status"
                  optionFilterProp="children"
                  onChange={(value) => {
                    handleOnChange({
                      ...filter,
                      status: value
                    });
                  }}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  <Option value="activated">Activated</Option>
                  <Option value="deactivated">Deactivated</Option>
                </Select>
              </Col>
              <Col md={10} sm={12}>
                <Input
                  name="search_term"
                  size="large"
                  className="search-input"
                  style={{ width: "100%" }}
                  placeholder="Search..."
                  onChange={onChange}
                  value={filter?.search_term}
                  autoComplete="off"
                  allowClear={true}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Filters;
