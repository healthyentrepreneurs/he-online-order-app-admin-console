import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import pageTitles from "../../../../util/page-titles";
import { OpenNotificationWithIcon } from '../../../../reusable-components/Notifications';
import { Helmet } from 'react-helmet';
import Filters from "./filters";
import List from "./list";
import { message, Modal, Row, Col } from "antd";
import QrcodeImage from './QrCodeImage'
import SyncProgressModal from './sync-progress-modal';
import SyncUserError from './sync-user-error';
import moment from 'moment';


const ListComponent = (props) => {
  const [isVisible, setIsVisible] = useState(false)
  const [userID, setUserID] = useState(null)
  const [userName, setUserName] = useState(null)
  const [selectedUsers, setSelectedUsers] = useState([])
  const [qrCodeVisible, setQrCodeVisible] = useState(false)
  const [selectedUser, setSelectedUser] = useState(null)
  const [syncVisible, setSyncVisible] = useState(null)
  const [filters, setFilters] = useState(null)
  const [syncErrorVisible, setSyncErrorVisible] = useState(false)

  //props
  const { fetchUsers, fetchCountries, resetPagination, 
    fetchDistricts, fetchClusters, filter, resetClusters, 
    resetDistricts, generateChesActivationQrcode, 
    generatedChesActivationQrcode, generatingQrcode,
    syncUsers,
    syncingUsers,
    syncUsersMessage,
    syncUserStatus,
    syncPercent,
    trackUserSync,
    trackingUserSync,
    syncTaskId,
    loadingLastSync,
    lastSyncStatus,
    lastSyncTime,
    getLastSync,
    lastSyncData
  } = props;

  const initPage = () => {
    fetchCountries();
    if(props?.filter?.company_id) {
      fetchDistricts({ query: props?.filter?.company_id });
      fetchClusters()
    }
    if(props?.users?.length < 1) {
      resetPagination();
      fetchUsers();
    }
  }

  // const periodicUserSyncTracker = setInterval(() => {
  //   console.log('---scheduler about to run----')
  //   if(syncTaskId) {
  //     props.trackUserSync(syncTaskId)
  //   } else {
  //     console.log('tracker id is undefined')
  //   }
  // }, 18000);

  const handleOk = () => {
    const { deactivateUser } = props;
    deactivateUser(userID);
    setIsVisible(false)
  };

  const handleCancel = () => {
    setIsVisible(false)
  };

  const handleUserSelect = (selectedUsers) => {
    setSelectedUsers(selectedUsers)
  }

  const handleDownloadPasswords = (event, filetype) => {
    if(selectedUsers.length <= 1) {
      OpenNotificationWithIcon('error', 'Generate user passwords', 'Please select users!')
    } else {
      props.generateChesPasswords(selectedUsers)
    }
  }

  const handleOnQrcodeBtnClick = (user) => {
    const payload = {
      email: user?.email
    }
    setSelectedUser(user)
    setQrCodeVisible((visible) => {
      return !visible
    })
    generateChesActivationQrcode(payload)
  }

  useEffect(() => {
    initPage()
    getLastSync()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if(filter?.company_id) {
      fetchClusters({ country_id: filter?.company_id });
      fetchDistricts({ country_id: filter?.company_id });
    } else {
      resetClusters()
      resetDistricts()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filter?.company_id])

  useEffect(() => {
    let myInterval = setInterval(() => {
      if(syncingUsers && syncTaskId && !trackingUserSync) {
        console.log('--------> About to track users sync')
        trackUserSync(syncTaskId, filters)
      }
    }, 30000)
    return () => {
      clearInterval(myInterval)
    }
    //eslint-disable-next-line
  }, [syncingUsers, syncTaskId, syncUserStatus, trackingUserSync])

  useEffect(() => {
    if(!syncingUsers) {
      setSyncVisible(false)
    }
  }, [syncingUsers])

  useEffect(() => {
    let myInterval = setInterval(() => {
      if(!syncingUsers) {
        getLastSync()
      }
    }, 30000)
    return () => {
      clearInterval(myInterval)
    }
    //eslint-disable-next-line
  }, [loadingLastSync, lastSyncStatus, lastSyncTime, syncingUsers])


  return (
    <>
      <Helmet>
        <title>{props.pageTitle}</title>
      </Helmet>
      <Filters
        handleDownloadPasswords={handleDownloadPasswords}
        generatePasswordLoading={props.generatingChesPasswords}
        handleChange={(filterObj) => {
          //this.setState({ searchTerm, status, country, district: districtId, cluster: clusterId, selectedUsers: [] });
          resetPagination();
          fetchUsers({
            page: 1,
            ...filterObj
          });
        }}
        onSyncClick={(values) => {
          setSyncVisible(true)
          if(!syncingUsers) {
            setFilters(values)
            syncUsers(values)
          } else {
            message.info('Syncing in progress')
          }
          
        }}
        onDisplaySyncError={() => {
          setSyncErrorVisible(true)
        }}
        {...props}
      />
      <div className="filters-list">
          <List
            onHandleUserSelection={handleUserSelect}
            selectedUsers={selectedUsers}
            searchTerm={filter?.search_term}
            country={filter?.company_id}
            status={filter?.status}
            district={filter?.district_id}
            cluster={filter?.cluster_id}
            _openModal={(userID, userName) => {
              setIsVisible(true)
              setUserID(userID)
              setUserName(userName)
            }}
            handleOnQrcodeBtnClick={handleOnQrcodeBtnClick}
            selectedUser={selectedUser}
            generatingQrcode={generatingQrcode}
            {...props}
          />
        </div>
        <Modal
          title="Deactivate User"
          visible={isVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <p>
            Would you like to deactivate{" "}
            <span style={{ fontWeight: "bold" }}>{userName}</span> ?
          </p>
        </Modal>
        <Modal
          title="Activate by QR-code"
          visible={qrCodeVisible}
          footer={null}
          onCancel={() => {
            setQrCodeVisible(false)
            setSelectedUser(null)
          }}
        >
          <QrcodeImage 
            qrcodeSrc={generatedChesActivationQrcode} 
            loading={generatingQrcode} 
            name={selectedUser?.name}
            phoneNumber={selectedUser?.phone_number}
          />
        </Modal>
        <SyncProgressModal 
          visible={syncVisible}
          percent={syncPercent}
          status='normal'
          onCancel={() => {
            setSyncVisible(false)            
          }}
          message={syncUsersMessage}
          loading={syncingUsers}
        />
        <Modal
          visible={syncErrorVisible}
          title="Sync Error Details"
          onCancel={() => setSyncErrorVisible(false)}
          footer={null}
          style={{ minWidth: '800px', resize: 'none' }}
        >
          <SyncUserError
            data={lastSyncData}
          />
          <Row>
            <Col span={24} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <span>Timestamp: {moment(lastSyncTime).format('DD-MM-YYYY hh:mm A')}</span>
            </Col>
          </Row>
        </Modal>
    </>
  )
}

ListComponent.defaultProps = {
  pageTitle: pageTitles.listChes
}

ListComponent.propTypes = {
  isloading: PropTypes.bool,
};

export default ListComponent

