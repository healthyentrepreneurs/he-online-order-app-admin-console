
import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'

const LastSyncStatus = ({
  lastSyncTime,
  lastSyncStatus,
  onDisplayError,
}) => {

  return (
    <div style={{width: '100%', fontSize: 14, display: 'flex', flexDirection: 'column', height: 50, backgroundColor: '#f5faf5', padding: 5, boxSizing: 'border-box'}}>
      {(lastSyncTime && lastSyncStatus !== null) ? (
        <>
          <div style={{ width: '100%', height: '50%', display: 'flex', justifyContent: 'flex-start', alignItems: 'flex-end' }}>
            <span style={{ whiteSpace: 'nowrap' }}>
              { lastSyncStatus ? 'Sync completed at' : 'Sync error' }
            </span>
          </div>
          <div style={{ width: '100%', height: '50%', display: 'flex', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
            { lastSyncStatus ? (
              <span style={{ whiteSpace: 'nowrap' }}>
                { moment(lastSyncTime).format('DD-MM-YYYY hh:mm A') }
              </span>
            ) : (
              <div 
                style={{ width: 'auto', backgroundColor: 'orange', padding: 1 }} 
                onClick={() => {
                  onDisplayError()
                }}
              >
                <span style={{ fontSize: 8 }}>View error</span>
              </div>
            )}

          </div>
        </>
      ) : (
        <div style={{ display: 'flex', width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
          <span> No sync data </span>
        </div>
      )}
    </div>
  )
}

LastSyncStatus.propTypes = {
  lastSyncTime: PropTypes.string,
  lastSyncStatus: PropTypes.bool,
  lastSyncData: PropTypes.object,
  onDisplayError: PropTypes.func
}

export default LastSyncStatus