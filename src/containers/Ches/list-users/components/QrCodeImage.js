import { Col, Row, Spin } from 'antd'
import React from 'react'


const QrcodeImage = (props) => {
  const { qrcodeSrc, loading, phoneNumber, name } = props

  const getUrlFromSvg = (svgStr) => {
    const svg = new Blob([svgStr], { type: "image/svg+xml" });
    return URL.createObjectURL(svg);
  }

  return (
    <Row 
      style={{width: '100%', height: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}
    >
      <Col 
        span={24} 
        style={{width: '100%', height: '100%', display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}
      >
        {loading ? <Spin /> : (
          <Row>
            <Col span={24}>
              <Row style={{ width: '100%' }}>
                <Col span={24} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                  <img id='barcode' 
                    // src="https://api.qrserver.com/v1/create-qr-code/?data=HelloWorld&amp;size=100x100" 
                    // src={qrcodeSrc}
                    src={getUrlFromSvg(qrcodeSrc)}
                    alt="qr_code" 
                    title="Qr-Code" 
                    width="300" 
                    height="300" 
                  />
                </Col>
              </Row>
              <Row style={{marginTop: 10}}>
                <Col span={24} style={{ textAlign: 'center' }}>
                  <p>
                    Scan to activate user account
                  </p>
                  <p>
                    {phoneNumber} - {name}
                  </p>
                </Col>
              </Row>
            </Col>
          </Row>
        )}
      </Col>
    </Row>
  )
}

export default QrcodeImage