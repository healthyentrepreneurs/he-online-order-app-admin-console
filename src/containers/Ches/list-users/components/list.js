import React from "react";
import { Row, Col, Table, Button } from "antd";
import { EditOutlined, QrcodeOutlined } from "@ant-design/icons";
import Spinner from "../../../../reusable-components/Spinner";
import Pagination from "../../../../reusable-components/Pagination";
import { history } from "../../../../util/helpers/browserHistory";
import DeleteIcon from "../../../../assets/delete.svg";
import get from "lodash/get";

const columns = (props) => [
  {
    title: "ID",
    dataIndex: "odoo_id",
    key: "odoo_id",
    render: (text, row) => {
      return `#${get(row, "odoo_id")}`;
    },
  },
  {
    title: "Username",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Phone Number",
    dataIndex: "phone_number",
    key: "phone_number",
  },
  {
    title: "Company",
    dataIndex: "country.company_name",
    key: "country.company_name",
    render: (text, row) => {
      return row?.country?.company_name;
    },
  },
  {
    title: "District/Cluster",
    dataIndex: "district",
    key: "district",
  },
  {
    title: "Status",
    dataIndex: "is_active",
    key: "is_active",
    render: (text, row) => {
      if (row?.is_active) return <span style={{ color: "green" }}>activated</span>;
      else return <span style={{ color: "red" }}>deactivated</span>;
    },
  },
  {
    title: "Contact Type",
    dataIndex: "contact_type",
    key: "contact_type"
  },
  {
    title: "Action",
    dataIndex: "action",
    key: "action",
    align: "center",
    render: (text, row) => {
      return (
        <>
          <QrcodeOutlined 
            style={{ color: "#A9B4C1", paddingRight: "10px" }}
            className="edit-icon"
            onClick={() => {
              props.handleOnQrcodeBtnClick(row)
            }}
          />
          <EditOutlined
            onClick={() =>
              history.push({
                pathname: "/app/view/che",
                search: "",
                state: { id: row.id },
              })
            }
            style={{ color: "#A9B4C1", paddingRight: "10px" }}
            className="edit-icon"
          />

          <Button
            type="text"
            onClick={(e) => {
              e.preventDefault();
              props._openModal(row?.id, row?.name);
            }}
            style={{ padding: 0, height: 0 }}
          >
            <img src={DeleteIcon} alt="delete icon" />
          </Button>
        </>
      );
    },
  },
];




const List = ({
  searchTerm,
  status,
  country,
  users,
  page,
  totalCount,
  isloading,
  district,
  cluster,
  ...props
}) => {
  
  const { onHandleUserSelection, selectedUsers } = props

  const onSelect = (record, selected) => {
    const selectedRowId = record?.odoo_id;
    if(!selected) {
      onHandleUserSelection((selectedUsers || []).filter(item=> item !== selectedRowId))
    } else {
      if(!selectedUsers.includes(selectedRowId)) {
        onHandleUserSelection((selectedUsers || []).concat([selectedRowId]))
      }
    }
  }

  const onSelectAll = (selected, selectedRows, changeRows) => {
    const rowKeys = changeRows?.map(item => item?.odoo_id)
    const changeRowsKeys = changeRows?.map(item => item?.odoo_id)
    //handle all checked 
    if(selected) {
      const filtered = changeRowsKeys.filter(item=> !selectedUsers.includes(item))
      onHandleUserSelection(selectedUsers.concat(filtered))
    } else {
      const filterd = selectedUsers.filter(item=> !rowKeys.includes(item))
      onHandleUserSelection(filterd)
    }
  }

  return (
    <>
      {isloading ? (
        <Spinner />
      ) : (
        <>
          <Row>
            <Col span={24}>
              {users && (
                <Table
                  type="checkbox"
                  rowSelection={{
                    type: "checkbox",
                    onSelect,
                    onSelectAll,
                    preserveSelectedRowKeys: true,
                    selectedRowKeys: props?.selectedUsers || []
                  }}
                  className="table he-table"
                  dataSource={users || []}
                  pagination={false}
                  columns={columns(props)}
                  rowKey="odoo_id"
                />
              )}
            </Col>
          </Row>
          <Row>
            <Col offset={16} md={8}>
              <div className="filters-pagination">
                <Pagination
                  handlePagination={(page) => {
                    props.fetchUsers({
                      page,
                      search_term: searchTerm,
                      status,
                      company_id: country,
                      district_id: district,
                      cluster_id: cluster
                    });
                  }}
                  page={page}
                  totalCount={totalCount}
                  defaultPageSize={50}
                  modifyPagination={props.modifyPagination}
                />
              </div>
            </Col>
          </Row>
        </>
      )}
    </>
  );
};

export default List;
