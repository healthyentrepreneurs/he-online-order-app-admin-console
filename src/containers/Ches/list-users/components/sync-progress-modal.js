import { Col, Modal, Progress, Row, Spin } from 'antd'
import React from 'react'
import PropTypes from 'prop-types'

const SyncProgressModal = ({
  visible,
  percent,
  status='',
  onCancel,
  message='Syncing Users',
  loading
}) => {
  
  return (
    <Modal
      visible={visible}
      onCancel={() => onCancel()}
      title={(
        <Row>
          <Col span={24} style={{ textAlign: 'center' }}>
            <span style={{ marginRight: 5 }}>Sync Users</span>
            {loading && <Spin/>}
          </Col>
        </Row>
      )}
      footer={false}
    >
      <Row>
        <Col span={24} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 300 }}>
          <Progress
            type='circle'
            percent={percent}
            size='default'
            status={status}
            strokeColor={{ '0%': '#108ee9', '100%': '#87d068' }}
          />
        </Col>
      </Row>
      <Row>
        <Col span={24} style={{ textAlign: 'center' }}>
          <h4>{message}</h4>
        </Col>
      </Row>
    </Modal>
  )
}

SyncProgressModal.propTypes = {
  status: PropTypes.oneOf(['normal','success','exception']),
  visible: PropTypes.bool,
  onCancel: PropTypes.func,
  percent: PropTypes.number.isRequired
}

export default SyncProgressModal

