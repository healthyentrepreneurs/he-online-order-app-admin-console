import React, { Component } from "react";
import { Modal } from "antd";
import Filters from "./filters";
import List from "./list";

import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import pageTitles from "../../../../util/page-titles";
import { OpenNotificationWithIcon } from "../../../../reusable-components/Notifications";

export default class ListComponent extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   isVisible: false,
    //   userID: null,
    //   searchTerm: null,
    //   status: undefined,
    //   country: undefined,
    //   district: undefined,
    //   cluster: undefined,
    //   selectedUsers: []
    // };
    this.state = {
      isVisible: false,
      userID: null,
      selectedUsers: []
    };
  }

  async componentDidMount() {
    const { fetchUsers, fetchCountries, resetPagination, fetchDistricts, fetchClusters } = this.props;
    const { country } = this.state
    resetPagination();
    await fetchCountries();
    if(country) {
      await fetchDistricts({ query: this.state.country });
      await fetchClusters()
    }
    fetchUsers();
  }

  componentDidUpdate(prevProps, prevState) {
    const { isDeactivated, page, fetchUsers, fetchClusters, fetchDistricts, resetClusters, resetDistricts } = this.props;
    //const { searchTerm, status, country } = this.state;
    
    
    if(country !== prevState.country) {
      this.setState({ cluster: undefined, district: undefined });
      if(country) {
        fetchClusters({ country_id: country });
        fetchDistricts({ country_id: country });
      } else {
        resetClusters()
        resetDistricts()
      }
    }
    
    if (isDeactivated !== prevProps.isDeactivated) {
      if (isDeactivated) {
        fetchUsers({
          page,
          search_term: searchTerm,
          status,
          company_id: country,
        });
      }
    }
  }

  handleOk = () => {
    const { deactivateUser } = this.props;
    deactivateUser(this.state.userID);
    this.setState({ isVisible: false });
  };

  handleCancel = () => {
    this.setState({ isVisible: false });
  };

  handleUserSelect = (selectedUsers) => {
    this.setState({ selectedUsers })
  }

  handleDownloadPasswords = (event, filetype) => {
    const { selectedUsers } = this.state
    if(selectedUsers.length <= 1) {
      OpenNotificationWithIcon('error', 'Generate user passwords', 'Please select users!')
    } else {
      this.props.generateChesPasswords(selectedUsers)
    }
  }

  render() {
    const { fetchUsers, resetPagination } = this.props;
    const { searchTerm, status, country, userName, district, cluster } = this.state;

    return (
      <>
        <Helmet>
          <title>{this.props.pageTitle}</title>
        </Helmet>
        <Filters
          handleDownloadPasswords={this.handleDownloadPasswords}
          generatePasswordLoading={this.props.generatingChesPasswords}
          handleChange={(country, status, searchTerm, districtId, clusterId) => {
            this.setState({ searchTerm, status, country, district: districtId, cluster: clusterId, selectedUsers: [] });
            resetPagination();
            const { filter } = props
            fetchUsers({
              page: 1,
              search_term: filter?.search_term,
              company_id: filter?.company_id,
              status,
              district_id: filter?.district_id,
              cluster_id: filter?.cluster_id
            });
          }}
          {...this.props}
        />
        <div className="filters-list">
          <List
            onHandleUserSelection={this.handleUserSelect}
            selectedUsers={this.state.selectedUsers}
            searchTerm={searchTerm}
            country={country}
            status={status}
            district={district}
            cluster={cluster}
            _openModal={(userID, userName) => {
              this.setState({ isVisible: true, userID, userName });
            }}
            {...this.props}
          />
        </div>
        <Modal
          title="Deactivate User"
          visible={this.state.isVisible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>
            Would you like to deactivate{" "}
            <span style={{ fontWeight: "bold" }}>{userName}</span> ?
          </p>
        </Modal>
      </>
    );
  }
}

ListComponent.defaultProps = {
  pageTitle: pageTitles.listChes
}

ListComponent.propTypes = {
  isloading: PropTypes.bool,
};
