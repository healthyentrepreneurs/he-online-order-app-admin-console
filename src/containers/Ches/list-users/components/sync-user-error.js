import React from 'react'
import PropTypes from "prop-types"
import { Col, Row } from 'antd'

const getCountryList = (data) => {
  console.log('country data', data)
  let countries = []
  for (const key in data) {
    
    if(data.hasOwnProperty(key) && (typeof data[key] === 'object')) {
      let obj = {name: key, errors: data[key]?.errors, id: data[key]?.company_id}
      if(data[key]?.errors.length > 0) {
        countries.push(obj)
      }
    }
  }
  console.log('countries', countries)
  return countries
}

const SyncUserError = ({
  data
}) => {
  
  return (
    <Row>
      <Col span={24}>
        {
          (getCountryList(data) || []).map(country => {
            return (
              <Row key={country?.id} style={{ marginBottom: 20, border: 'green solid 1px' }}>
                <Col span={24}>
                  <Row>
                    <Col span={24}>
                      <p>
                        <span>
                          Records: {`${country?.name} (id=${country?.id})`}
                        </span>
                      </p>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={24}>
                      <span>Errors:</span>
                      <ul>
                        {country?.errors.map((error,index) => (
                          <li key={`${country?.id}-${index}`}>
                            {error}
                          </li>
                        ))}
                      </ul>
                    </Col>
                  </Row>
                
                </Col>
              </Row>
            )
          })
        }
      </Col>
    </Row>
  )
}

SyncUserError.propTypes = {
  country: PropTypes.string,
  errors: PropTypes.array
}
export default SyncUserError