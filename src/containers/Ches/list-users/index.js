import { connect } from "react-redux";
import { Creators } from "../../../services/redux/users/actions";
import { Creators as PaginationCreators } from "../../../services/redux/pagination/actions";
import { Creators as CountryCreators } from "../../../services/redux/country/actions";
import { Creators as DistrictsCreators } from "../../../services/redux/districts/actions" 
import { Creators as ClustersCreators } from "../../../services/redux/clusters/actions"
import ListComponent from "./components";

const mapStateToProps = (state) => {
  const { 
    users, isloading, totalCount, isDeactivated, 
    generatedChesPasswords, generatingChesPasswords, 
    refreshing, filter, generatedChesActivationQrcode, generatingQrcode, 
    generateQrcodeSuccess, 
    syncingUsers,
    syncUsersMessage,
    syncUserStatus,
    syncTotal,
    syncPending,
    syncPercent,
    syncTaskId,
    trackingUserSync,
    loadingLastSync,
    lastSyncStatus,
    lastSyncTime,
    lastSyncData
  } = state.users;

  return {
    users,
    isloading,
    pagination: state.pagination,
    page: state.pagination.currentPage,
    countries: state.country.countries,
    countriesLoading: state.country.isloading,
    totalCount,
    isDeactivated,
    districts: state.districts.districts,
    districtsLoading: state.districts.isLoading,
    clusters: state.clusters.clusters,
    clustersLoading: state.clusters.isLoading,
    generatedChesPasswords,
    generatingChesPasswords,
    refreshing,
    filter,
    generatedChesActivationQrcode,
    generatingQrcode,
    generateQrcodeSuccess,
    syncingUsers,
    syncUsersMessage,
    syncUserStatus,
    syncTotal,
    syncPending,
    syncPercent,
    syncTaskId,
    trackingUserSync,
    loadingLastSync,
    lastSyncStatus,
    lastSyncTime,
    lastSyncData
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUsers: (query = { page: 1 }) => {
      dispatch(Creators.fetchUsers(query));
    },
    fetchCountries: () => {
      dispatch(CountryCreators.fetchCountries());
    },
    deactivateUser: (id) => {
      dispatch(Creators.deactivateUser(id));
    },
    modifyPagination: (obj) => {
      dispatch(PaginationCreators.modifyPagination(obj));
    },
    resetPagination: () => {
      dispatch(PaginationCreators.reset());
    },
    fetchDistricts: (query) => {
      dispatch(DistrictsCreators.fetchDistricts(query))
    },
    resetDistricts: () => {
      dispatch(DistrictsCreators.resetDistricts())
    },
    fetchClusters: (query) => {
      dispatch(ClustersCreators.fetchClusters(query))
    },
    resetClusters: () => {
      dispatch(ClustersCreators.resetClusters())
    },
    generateChesPasswords: (userIds) => {
      dispatch(Creators.generateChesPasswords(userIds))
    },
    refreshUsers: (query) => {
      dispatch(Creators.refreshUsers(query))
    },
    syncUsers: (query) => {
      dispatch(Creators.syncUsers(query))
    },
    trackUserSync: (taskId, query) => {
      dispatch(Creators.trackUserSync(taskId, query))
    },
    setFilter: (filter) => {
      dispatch(Creators.setFilter(filter))
    },
    resetFilter: (filter) => {
      dispatch(Creators.resetFilter())
    },
    generateChesActivationQrcode: (payload) => {
      dispatch(Creators.generateChesActivationQrcode(payload))
    },
    getLastSync: () => {
      dispatch(Creators.getLastSync())
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListComponent);
