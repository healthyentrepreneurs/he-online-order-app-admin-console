import { connect } from "react-redux";
import { Creators } from "../../../services/redux/users/actions";
import ViewUser from "./components";

import "./styles/view-che.less";

const mapStateToProps = (state) => {
  const {
    user,
    isloading,
    isFetching,
    isPasswordReset,
    generated_password,
  } = state.users;
  return {
    user,
    isloading,
    isFetching,
    isPasswordReset,
    generated_password,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUser: (id) => {
      dispatch(Creators.getUser(id));
    },
    resetPassword: (id) => {
      dispatch(Creators.resetChePassword(id));
    },
    resetPasswordGeneration: () => {
      dispatch(Creators.resetPasswordGeneration());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewUser);
