import React, { useState, useRef } from "react";
import Spinner from "../../../../reusable-components/Spinner";

const ResetPassword = ({
	isloading,
	user,
	isPasswordReset,
	generated_password,
	...props
}) => {
	const [copySuccess, copyText] = useState(false);
	let inputEl = useRef(null);

	let buttonState = isloading ? <Spinner color="#ffffff" /> : "Generate Password";
	let copyButtonState = copySuccess ? "Copied to clipboard!" : "Copy to clipboard";

	const copyToClipboard = () => {
		inputEl.current.select();
		document.execCommand("copy");
		inputEl.current.focus();
		copyText(true);
	};

	return (
		<>
			<div className="reset-password">
				{isPasswordReset ? (
					<>
						<span className="textCenter">
							A password has been created. Give the healthy entrepreneur so
						</span>
						<span className="textCenter">that they can create a new PIN.</span>
						<span
							className="clipboard"
							style={{ fontWeight: "bold", margin: "19px 0 0 0", fontSize: "16px" }}
						>
							<strong>{generated_password}</strong>
						</span>
						<input
							className="clipboard-input"
							ref={inputEl}
							type="text"
							value={generated_password}
						/>
					</>
				) : (
					<>
						<span className="textCenter">
							Click on the button below to generate a password which you will
						</span>
						<span className="textCenter">
							provide the healthy entrepreneur so that they can create a new PIN.
						</span>
					</>
				)}
				<div className="button-container">
					{isPasswordReset ? (
						<button className="copy-button" onClick={copyToClipboard}>
							{copyButtonState}
						</button>
					) : (
						<button
							className="copy-button"
							onClick={() => props.resetPassword(user && user.id)}
						>
							{buttonState}
						</button>
					)}
				</div>
			</div>
		</>
	);
};

export default ResetPassword;
