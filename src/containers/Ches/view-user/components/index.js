import React, { Component } from "react";
import { Row, Col, Tabs, Breadcrumb } from "antd";
import PropTypes from "prop-types";
import ResetPassword from "./ResetPassword";
import get from "lodash/get";
import Spinner from "../../../../reusable-components/Spinner";
import { history } from "../../../../util/helpers/browserHistory";
import { Helmet } from "react-helmet";
import pageTitles from "../../../../util/page-titles";

const { TabPane } = Tabs;

export default class ViewUser extends Component {
  componentDidMount() {
    const { fetchUser, resetPasswordGeneration, location } = this.props;
    resetPasswordGeneration();
    fetchUser(get(location, "state.id"));
  }

  callback = (key) => {
    console.log(key);
  };

  render() {
    const { user, isFetching } = this.props;
    return (
      <>
        <Helmet>
          <title>{this.props.pageTitle}</title>
        </Helmet>
        {isFetching ? (
          <Spinner />
        ) : (
          <Row>
            <Col span={24} className="bread-crumb-container">
              {" "}
              <Breadcrumb>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>
                  <a
                    href="true"
                    onClick={(e) => {
                      e.preventDefault();
                      history.push("/app/ches");
                    }}
                  >
                    Users List
                  </a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>User Details</Breadcrumb.Item>
              </Breadcrumb>
            </Col>
            <Col span={24}>
              <h1
                style={{
                  fontSize: "36px",
                  fontWeight: "bold",
                }}
              >
                User ID - #{user && user.odoo_id}
              </h1>
            </Col>
            <Col span={24}>
              <div className="user-details-container">
                <div>
                  <p className="detail-headers">USER ID</p>
                  <p className="detail-desc">{get(user, "odoo_id") || "N/A"}</p>
                </div>
                <div>
                  <p className="detail-headers">USERNAME</p>
                  <p className="detail-desc">{get(user, "name") || "N/A"}</p>
                </div>
                <div>
                  <p className="detail-headers">PHONE NUMBER</p>
                  <p className="detail-desc">{get(user, "phone_number") || "N/A"}</p>
                </div>
                <div>
                  <p className="detail-headers">COUNTRY</p>
                  <p className="detail-desc">{get(user, "country.name") || "N/A"}</p>
                </div>
                <div>
                  <p className="detail-headers">DISTRICT</p>
                  <p className="detail-desc">{get(user, "district") || "N/A"}</p>
                </div>
                <div>
                  <p className="detail-headers">STATUS</p>
                  <p className="detail-desc">
                    {get(user, "is_active") ? "Activated" : "Deactivated"}
                  </p>
                </div>
              </div>
            </Col>
            <Col span={24}>
              <Tabs defaultActiveKey="1" onChange={this.callback}>
                <TabPane tab="Reset Password" key="2">
                  <ResetPassword {...this.props} />
                </TabPane>
              </Tabs>
            </Col>
          </Row>
        )}
      </>
    );
  }
}

ViewUser.defaultProps = {
  pageTitle: pageTitles.cheView
}

ViewUser.propTypes = {
  fetchUser: PropTypes.func,
};
