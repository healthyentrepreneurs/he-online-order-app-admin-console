import React, { useState } from "react";
import { Input, Select, Row, Col, DatePicker } from "antd";
import moment from "moment";
import { getPaymentMethodName } from "../../../../util/helpers/commonHelper";
import { debounce } from "lodash";

const { Option } = Select;

const Filters = ({ isloading, handleChange, paymentMethods, ...props }) => {

  const [ filters, setFilters ] = useState({
    search_term: "",
    start_date: "",
    end_date: "",
    status: "",
    provider: ""
  })


  const handleSearchInputChange = debounce((val) => {
    //setFilters({...filters, search_term: val});
    console.log('filter...', val)
    handleChange({...filters, search_term: val});
  }, 1000)

  // const onChange = (e) => {
  //   const val = e.target.value;
  //   setFilters({...filters, search_term: val});
  //   handleChange({...filters, search_term: val});
  // };

  return (
    <>
      <div>
        <Row gutter={32}>
          <Col sm={24} md={6}>
            <h1 className="header">Payments</h1>
          </Col>
          <Col md={18} sm={24}>
            <Row gutter={2}>
              <Col
                md={2}
                sm={12}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <h4 style={{ padding: "0", margin: "0" }}>Filter</h4>
              </Col>
              <Col md={4}>
                <Select
                  allowClear={true}
                  name="status"
                  size="large"
                  style={{ width: "100%" }}
                  loading={isloading}
                  showSearch
                  placeholder="status"
                  optionFilterProp="children"
                  onChange={(value) => {
                    setFilters({...filters, status: value});
                    handleChange({...filters, status: value});
                  }}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  <Option value="successful">Successful</Option>
                  <Option value="received">Received</Option>
                  <Option value="failed">Failed</Option>
                </Select>
              </Col>
              <Col md={4}>
                <Select
                  allowClear={true}
                  name="payment type"
                  size="large"
                  style={{ width: "100%" }}
                  loading={isloading}
                  showSearch
                  placeholder="payment option"
                  optionFilterProp="children"
                  onChange={(value) => {
                    setFilters({...filters, provider: value});
                    handleChange({...filters, provider: value});
                  }}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {paymentMethods.map(it => (
                    <Select.Option key={it?.id}>
                      { getPaymentMethodName(it?.name) }
                    </Select.Option>
                  ))}
                </Select>
              </Col>
              <Col md={4}>
                <DatePicker
                  name="from_date"
                  format="YYYY-MM-DD HH:mm:ss"
                  showTime={{ defaultValue: moment("00:00:00", "HH:mm:ss") }}
                  size="large"
                  className="filters-date-picker"
                  onChange={(date, dateString) => {
                    console.log('startDate', dateString)
                    setFilters({...filters, start_date: dateString});
                    handleChange({...filters, start_date: dateString});
                  }}
                  placeholder="Start date"
                  disabledDate={(current) => {
                    // disable future dates
                    const today = moment();
                    return current && current > moment(today, "YYYY-MM-DD");
                  }}
                />
              </Col>
              <Col md={4}>
                <DatePicker
                  name="to_date"
                  format="YYYY-MM-DD HH:mm:ss"
                  showTime={{ defaultValue: moment("00:00:00", "HH:mm:ss") }}
                  size="large"
                  className="filters-date-picker"
                  onChange={(date, dateString) => {
                    setFilters({...filters, end_date: dateString});
                    handleChange({...filters, end_date: dateString});
                  }}
                  placeholder="End date"
                  disabledDate={(current) => {
                    // disable future dates
                    const today = moment();
                    return current && current > moment(today, "YYYY-MM-DD");
                  }}
                />
              </Col>
              <Col md={6} sm={7}>
                <Input
                  name="search_term"
                  size="large"
                  className="search-input"
                  style={{ width: "100%" }}
                  placeholder="Search..."
                  onChange={(e) => {
                    const filter = e.target.value
                    setFilters({...filters, search_term: filter})
                    handleSearchInputChange(filter)
                  }}
                  value={filters.search_term}
                  autoComplete="off"
                  allowClear={true}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default Filters;
