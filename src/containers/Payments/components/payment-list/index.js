import React from "react";
import Filters from "./filters";
import List from "./list";
import { useState } from "react";
import { useEffect } from "react";


const PaymentListIndex = (props) => {
  console.log('index props', props)
  const { fetchPayments, resetPagination, isloading, payments, fetchPaymentMethods } = props;


  const [filter, setFilter] = useState({
    search_term: null,
    status: undefined,
    start_date: undefined,
    end_date: undefined
  })

  // const [isVisible, setIsVisible] = useState(false)
  // const [userID, setUserID] = useState(null)
  // const [searchTerm, setSearchTerm] = useState(null)
  // const [status, setStatus] = useState(undefined)
  // const [fromDate, setFromDate] = useState(undefined)
  // const [endDate, setEndDate] = useState(undefined)

  useEffect(() => {
    resetPagination();
    fetchPayments();
    fetchPaymentMethods();
    // eslint-disable-next-line
  }, [])

  return (
    <>
        <Filters
          handleChange={({start_date, end_date, status, search_term, provider}) => {
            setFilter({ ...filter, start_date, end_date, status, search_term, provider })
            resetPagination();

            
            const query = {
              page: 1,
              search_term,
              start_date,
              end_date,
              provider: provider,
              status,
            }
            fetchPayments(query);
          }}
          {...props}
        />
        <div className="filters-list">
          <List
            searchTerm={filter.searchTerm}
            start_date={filter.start_date}
            end_date={filter.end_date}
            status={filter.status}
            {...props}
            payments={payments}
            loading={isloading}
          />
        </div>
      </>
  )
}

export default PaymentListIndex
