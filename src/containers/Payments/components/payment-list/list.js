import React from "react";
import { Row, Col, Table, Spin } from "antd";
import Pagination from "../../../../reusable-components/Pagination";
import moment from "moment";
import { Helmet } from "react-helmet";
import pageTitles from "../../../../util/page-titles";
import { getPaymentMethodName } from "../../../../util/helpers/commonHelper";


const columns = [
  {
    title: "Date/Time",
    dataIndex: "created_at",
    key: "created_at",
    render: (text, row) => {
      return moment(row.created_at).format("YYYY-MM-DD hh:mm:ss A");
    },
  },
  {
    title: "User",
    dataIndex: "user",
    key: "user",
    render: text => text || 'N/A'
  },
  {
    title: "Phone Number",
    dataIndex: "phone_number",
    key: "phone_number",
    render: text => text || 'N/A'
  },
  {
    title: "Payment Option",
    dataIndex: "payment_method",
    key: "payment_method",
    render: text => getPaymentMethodName(text || 'N/A')
  },
  {
    title: "Transaction ID",
    dataIndex: "transaction_id",
    key: "transaction_id",
    render: text => text || 'N/A'
  },
  {
    title: "Invoice Nr",
    dataIndex: "invoice_id",
    key: "invoice_id",
    render: text => text || 'N/A'
  },
  {
    title: "Type",
    dataIndex: "payment_type",
    key: "payment_type",
    render: text => text || 'N/A'
  },
  {
    title: "Status",
    dataIndex: "status",
    key: "status",
  }
];

const List = ({
  searchTerm,
  start_date,
  end_date,
  status,
  payments,
  page,
  totalCount,
  loading,
  ...props
}) => {
  const { pageTitle } = props
  return (
    <>
      <Helmet>
        <title>{pageTitle}</title>
      </Helmet>
      {loading ? (
        <Spin />
      ) : (
        <>
          <Row>
            <Col span={24}>
                <Table
                  className="table he-table"
                  dataSource={payments}
                  pagination={false}
                  columns={columns}
                  rowKey="id"
                  loading={loading}
                />
            </Col>
          </Row>
          <Row>
            <Col offset={16} md={8}>
              <div className="filters-pagination">
                <Pagination
                  handlePagination={(page) => {
                    props.fetchPayments({
                      page,
                      search_term: searchTerm,
                      status,
                      start_date: start_date,
                      end_date: end_date,
                    });
                  }}
                  page={page}
                  totalCount={totalCount}
                  modifyPagination={props.modifyPagination}
                />
              </div>
            </Col>
          </Row>
        </>
      )}
    </>
  );
};

List.defaultProps = {
  pageTitle: pageTitles.paymentsList
}

export default List;
