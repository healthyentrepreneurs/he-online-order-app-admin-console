import { connect } from "react-redux";
import { Creators } from "../../services/redux/payments/actions";
import { Creators as PaginationCreators } from "../../services/redux/pagination/actions";
import { Creators as PaymentMethodCreators} from "../../services/redux/payment_methods/actions"
import PaymentListIndex from './components/payment-list/index'

const mapStateToProps = (state) => {
  const { payments, isloading, totalCount } = state.payments;
  return {
    payments,
    isloading,
    pagination: state.pagination,
    page: state.pagination.currentPage,
    totalCount,
    loadingPaymentMethods: state.payment_methods.isloading,
    paymentMethods: state.payment_methods.payment_methods
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchPayments: (query = { page: 1 }) => {
      dispatch(Creators.fetchPayments(query));
    },
    modifyPagination: (obj) => {
      dispatch(PaginationCreators.modifyPagination(obj));
    },
    resetPagination: () => {
      dispatch(PaginationCreators.reset());
    },
    fetchPaymentMethods: () => {
      dispatch(PaymentMethodCreators.fetchPaymentMethods())
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentListIndex);