import React, { useEffect } from 'react';
import { Table, Button as AntButton, Spin, Popconfirm } from 'antd';
import { EditOutlined } from '@ant-design/icons';

import Button from '../../../../reusable-components/Button';
import DeleteIcon from '../../../../assets/delete.svg';
import '../../../../styles/staff.less'
import { history } from '../../../../util/helpers/browserHistory';
import { Helmet } from 'react-helmet';
import pageTitles from '../../../../util/page-titles';

const columns = [
    {
      title: 'Full Name',
      dataIndex: 'name',
      align: 'left',
      width: '20%'
    },
    {
      title: 'Email Address',
      dataIndex: 'email',
      align: 'left',
      width: '25%'
    },
    {
      title: 'Role',
      dataIndex: 'role',
      align: 'left',
      width: '10%'
    },
    {
        title: 'Company',
        dataIndex: 'company',
        align: 'left',
        width: '20%'
    },
    {
        title: 'Action',
        dataIndex: 'action',
        align: 'center',
        width: '10%'
    }
];

const StaffList = props => {
    const { pageTitle } = props
    useEffect(() => {
        props.getStaff();
        // eslint-disable-next-line
    }, []);

    const actions = (id, data) => {
        const { isDeleting, selectedId } = props;
        return (
            <div className="action-icon">
                <AntButton type="text" onClick={() => history.push(`/app/edit-staff/${id}`, { data })}>
                <EditOutlined style={{ color: '#A9B4C1' }} />
                </AntButton>
                <Popconfirm
                    title="Are you sure?"
                    okText="Yes"
                    cancelText="No"
                    onConfirm={() => props.deleteStaff(id)}
                >
                    {isDeleting && selectedId === id ? <Spin /> : <AntButton type="text" onClick={(e) => e.preventDefault()}><img src={DeleteIcon} alt="delete icon" /></AntButton>}
                </Popconfirm>
            </div>
        )
    }

    const formattedStaffs = () => {
        const { staffs } = props;
        let staffData = []
        if (staffs && staffs.count) {
            // eslint-disable-next-line
            staffs.results.map((data, index) => {
                if (data.is_staff) {
                    staffData.push({
                        key: index,
                        name: data.name,
                        email: data.email,
                        role: data.role,
                        company: data.country ? data.country.company_name : 'N/A',
                        country: data.country ? data.country.name : 'N/A',
                        action: actions(data.id, data)
                    })
                }
            })
        }
        return staffData
    }
    return (
        <>
            <Helmet>
                <title>{pageTitle}</title>
            </Helmet>
            <div className="staff-list">
                <div className="header">
                    <h4 className="title">Staff</h4>
                    <Button style={{ width: '250px' }} text="Add New Staff" onClick={() => history.push('/app/add-staff')} />
                </div>
                {props.isFetchingStaff ? <Spin /> : 
                    <Table
                        className="he-table"
                        columns={columns}
                        dataSource={formattedStaffs()}
                        pagination={false}
                        align="center"
                    />
                }
            </div>
        </>
    )
}

StaffList.defaultProps = {
    pageTitle: pageTitles.staffList
}

export default StaffList;
