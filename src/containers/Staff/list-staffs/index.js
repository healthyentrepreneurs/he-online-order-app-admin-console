import { connect } from 'react-redux';

import { Creators } from "../../../services/redux/staff/actions";
import ListStaff from './components';

const mapStateToProps = state => {
    return {
        isFetchingStaff: state.staff.isFetching,
        staffs: state.staff.staffs,
        isDeleting: state.staff.isDeleting,
        selectedId: state.staff.id
    }   
}

const mapDispatchToProps = dispatch => {
    return {
        getStaff: () => {
            dispatch(Creators.getStaffRequest())
        },
        deleteStaff: (id) => {
            dispatch(Creators.deleteStaffRequest(id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListStaff)
