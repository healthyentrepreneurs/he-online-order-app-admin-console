import { connect } from 'react-redux';

import { Creators } from '../../../services/redux/staff/actions';
import EditStaff from './components';

const mapStateToProps = state => {
    return {
        loading: state.staff.isEditing
    }
}

const mapDispatchToProps = dispatch => {
    return {
        submit: (id, data) => {
            dispatch(Creators.editStaffRequest(id, data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditStaff);
