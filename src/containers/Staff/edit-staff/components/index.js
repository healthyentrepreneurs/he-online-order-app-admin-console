import React from 'react';
import { Helmet } from 'react-helmet';
import pageTitles from '../../../../util/page-titles';

import StaffForm from '../../components/form';


const EditStaff = props => {
    const { data } = props.location.state;
    const { pageTitle } = props
    let initialValues = {};
    if (data) {
        initialValues = {
            name: data.name,
            email: data.email,
            country: data.country ? data.country.id : null,
            id: data.id
        }
    } 
    // fetch data from api if history is polluted
    return (
        <>
            <Helmet>
                <title>{pageTitle}</title>
            </Helmet>
            <StaffForm initialValues={initialValues} title="Edit Staff" {...props} />
        </>
    )
}

EditStaff.defaultProps = {
    pageTitle: pageTitles.staffEdit
}

export default EditStaff;
