import React, { useEffect } from 'react';
import { Card, Form, Row, Col, Input, Select } from 'antd';
import { connect } from 'react-redux';

import { Creators as CountryCreators } from "../../../services/redux/country/actions";
import '../../../styles/staff.less';
import Button from '../../../reusable-components/Button';

const { Option } = Select;

const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
    layout: "vertical",
};

const StaffForm = ({
    countries,
    fetchCountries,
    countryLoading,
    loading,
    initialValues,
    title,
    submit
}) => {
    useEffect(() => {
        fetchCountries();
        // eslint-disable-next-line
    }, [])

    return (
        <Form
            {...formItemLayout}
            onFinish={(values) => {
                if (initialValues) {
                    submit(initialValues.id, values)
                } else {
                    submit(values)
                }
            }}
            requiredMark={false}
            initialValues={initialValues}
        >
            <Card className="staff-form-wrapper">
                <div className="staff-form-content">
                <Row>
                    <Col span={24}>
                        <h4 className="title">{title}</h4>
                    </Col>
                    <Col span={24}>
                        <Form.Item
                            label="Full Name"
                            className="custom-label"
                            name="name"
                            rules={[
                                {
                                    required: true,
                                    message: "Full name is required"
                                }
                            ]}
                        >
                            <Input
                                placeholder="Enter the full name"
                                className="form-input"
                                size="large"
                            />
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Form.Item
                            label="Email"
                            className="custom-label"
                            name="email"
                            rules={[
                                {
                                    required: true,
                                    message: "Email is required"
                                },
                                {
                                    type: 'email',
                                    message: "Email is invalid"
                                }
                            ]}
                        >
                            <Input
                                placeholder="Enter the email address"
                                className="form-input"
                                size="large"
                            />
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                    </Col>
                    <Col span={24}>
                        <Form.Item
                            name="country"
                            label="Company"
                            rules={[
                                {
                                    required: true,
                                    message: "Country is required"
                                }
                            ]}
                        >
                            <Select
                                size="large"
                                style={{ width: "100%" }}
                                showSearch
                                loading={countryLoading}
                                placeholder="Select Company"
                                optionFilterProp="children"
                                filterOption={(input, option) =>
                                option.props.children
                                    .toLowerCase()
                                    .indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                {countries && countries.length > 0 && (
                                countries.map((country) => (
                                    <Option key={country.id} value={country.id}>
                                    {country.company_name}
                                    </Option>
                                ))
                                )}
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Form.Item>
                            <Button
                                style={{ width: '100%' }}
                                htmlType="submit"
                                text="Submit"
                                loading={loading}
                            />
                        </Form.Item>
                    </Col>
                </Row>

                </div>
            </Card>
        </Form>
    );
}

const mapStateToProps = state => {
    return {
        countries: state.country.countries,
        countryLoading: state.country.isloading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchCountries: () => {
            dispatch(CountryCreators.fetchCountries());
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StaffForm);
