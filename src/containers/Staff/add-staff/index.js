import { connect } from 'react-redux';

import { Creators } from '../../../services/redux/staff/actions';
import AddStaff from './components';

const mapStateToProps = state => {
    return {
        loading: state.staff.isAdding
    }
}

const mapDispatchToProps = dispatch => {
    return {
        submit: (data) => {
            dispatch(Creators.addStaffRequest(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddStaff);
