import React from 'react';
import { Helmet } from 'react-helmet';
import pageTitles from '../../../../util/page-titles';

import StaffForm from '../../components/form';


const AddStaff = props => {
    const { pageTitle } = props
    return (
        <>
            <Helmet>
                <title>{pageTitle}</title>
            </Helmet>
            <StaffForm title="Add New Staff" {...props} />
        </>
    )
}

AddStaff.defaultProps = {
    pageTitle: pageTitles.staffAdd
}

export default AddStaff;
