#!/usr/bin/env bash
pip install ansible

if [ -z "$1" ]; then
    if [ -f ./hosts ]; then
        HOSTS=hosts
    else
        echo "usage: $0 <system>"
        exit
    fi
else
    HOSTS=$1
fi

ansible-playbook  site.yml -i $HOSTS --tags deploy
